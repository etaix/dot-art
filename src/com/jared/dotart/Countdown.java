/**
 * 
 */
package com.jared.dotart;

/**
 * This class defines the countdown's data
 * @author Eric Taix (eric.taix@gmail.com)
 */
public class Countdown {
	
	// Countdown's type
	public enum Type {
		ABSOLUTE, RELATIVE, NONE;
	}
	
	private Type type = Type.NONE;
	
	/**
	 * Default constructor with default values
	 */
	public Countdown() {
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}
	
	
}
