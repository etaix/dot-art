/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;

/**
 * A MiscellaneaousListener is a class which is interested to any change of the miscellaneaous values (title, watch again button, ...)
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface MiscellaneaousListener extends Listener {

	/**
	 * A miscellaneaous value has changed
	 * @param evt
	 */
	public void miscellaneaousChanged(MiscellaneousEvent  evtP);
}
