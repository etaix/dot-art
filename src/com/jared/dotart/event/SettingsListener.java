/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;

/**
 * A SettingsListener is a class which is interested to any change of the countdown settings (veolicities, gravity, restitution)
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface SettingsListener extends Listener {

	/**
	 * The countdown's type has changed
	 * @param evt
	 */
	public void settingsChanged(SettingsEvent  evtP);
}
