/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;

/**
 * An image has been added
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public interface ImageListener extends Listener {

	/**
	 * An image has bee added to the current image list
	 * @param inputP
	 */
	public void imagesChanged(ImageEvent eventP);

}
