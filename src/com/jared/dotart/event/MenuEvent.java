/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Event;


/**
 * This event is fired when the main menu selection changed
 * @author Eric Taix (eric.taix@gmail.com)
 */
public class MenuEvent implements Event {
	
	private String feature;
	
	public MenuEvent(String featureP) {
		feature = featureP;
	}

	/**
	 * @return the menuId
	 */
	public String getFeature() {
		return feature;
	}
	
}
