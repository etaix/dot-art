/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;

/**
 * A CountdownListener is a class which is interested to any change of the countdown data
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface CountdownListener extends Listener {

	/**
	 * The countdown's type has changed
	 * @param evt
	 */
	public void countdownTypeChanged(CountdownTypeEvent  evtP);
}
