/**
 * 
 */
package com.jared.dotart.event;

import java.util.ArrayList;
import java.util.List;

import com.github.wolfie.blackboard.Event;
import com.jared.dotart.model.ImageSettings;

/**
 * This event is used when an image is added or removed
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class ImageEvent implements Event {

	private List<ImageSettings> items = new ArrayList<ImageSettings>();

	
	/**
	 * The default constructor. 
	 */
	public ImageEvent() {
	}

	/**
	 * Add a new item into the odered list
	 * @param item
	 */
	public void addItem(ImageSettings item) {
		items.add(item);
	}
	
	/**
	 * Return the image list
	 * @return
	 */
	public List<ImageSettings> getItems() {
		return items;
	}
}
