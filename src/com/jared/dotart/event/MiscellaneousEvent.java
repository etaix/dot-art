/**
 * 
 */
package com.jared.dotart.event;

import java.io.Serializable;

import com.github.wolfie.blackboard.Event;

/**
 * This event is fired when a miscellaneaous value has changed
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class MiscellaneousEvent implements Event, Serializable {

	private String title = "Dot-Art experience";
	private String watchAgainTitle = "Watch again";
	private String finalMessage = "";
	private String finalUrl = "";
	
	public MiscellaneousEvent() {
	}

	/**
	 * Set all dots color at the same time
	 * @param dc
	 * @param hc
	 * @param mc
	 * @param sc
	 * @param sepc
	 * @param oc
	 */
	public void setMiscellaneaous(String t, String wa, String fm, String fu) {
		title = t;
		watchAgainTitle = wa;
		finalMessage = fm;
		finalUrl = fu;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the watchAgainTitle
	 */
	public String getWatchAgainTitle() {
		return watchAgainTitle;
	}

	/**
	 * @param watchAgainTitle the watchAgainTitle to set
	 */
	public void setWatchAgainTitle(String watchAgainTitle) {
		this.watchAgainTitle = watchAgainTitle;
	}

	/**
	 * @return the finalMessage
	 */
	public String getFinalMessage() {
		return finalMessage;
	}

	/**
	 * @param finalMessage the finalMessage to set
	 */
	public void setFinalMessage(String finalMessage) {
		this.finalMessage = finalMessage;
	}

	/**
	 * @return the finalUrl
	 */
	public String getFinalUrl() {
		return finalUrl;
	}

	/**
	 * @param finalUrl the finalUrl to set
	 */
	public void setFinalUrl(String finalUrl) {
		this.finalUrl = finalUrl;
	}

}
