/**
 * 
 */
package com.jared.dotart.event;

import java.util.Date;

import com.github.wolfie.blackboard.Event;
import com.jared.dotart.Countdown.Type;

/**
 * This event is fired when the countdown's type has changed
 * @author Eric Taix (eric.taix@gmail.com)
 */
public class CountdownTypeEvent implements Event {

	private Type type = Type.RELATIVE;
	private Date date = null;;
	private int seconds = 15;

	public CountdownTypeEvent(Type typeP, Date dateP, int secondsP) {
		type = typeP;
		date = dateP;
		seconds = secondsP;
	}

	/**
	 * @return the countdown
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @return the seconds
	 */
	public int getSeconds() {
		return seconds;
	}
	
}
