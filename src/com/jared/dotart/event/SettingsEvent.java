/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Event;

/**
 * This event is fired when a setting changed (gravity, restitution, velocities)
 * @author Eric Taix (eric.taix@gmail.com)
 */
public class SettingsEvent implements Event {

	private double xGravity = 0d;
	private double yGravity = 400d;
	private double lowVelocity = 300d;
	private double highVelocity = 1000d;
	private double restitution = 1d;
	private double size = 18;
	private double xPos = 495;
	private double yPos = 307;
	
	public SettingsEvent() {
	}

	/**
	 * Set all dots color at the same time
	 * @param dc
	 * @param hc
	 * @param mc
	 * @param sc
	 * @param sepc
	 * @param oc
	 */
	public void setSettings(double xG, double yG, double lV, double hV, double r, double s, double xP, double yP) {
		xGravity = xG;
		yGravity = yG;
		lowVelocity = lV;
		highVelocity = hV;
		restitution = r;
		size = s;
		xPos = xP;
		yPos = yP;
	}

	/**
	 * @return the xGravity
	 */
	public double getxGravity() {
		return xGravity;
	}

	/**
	 * @return the yGravity
	 */
	public double getyGravity() {
		return yGravity;
	}

	/**
	 * @return the lowVelocity
	 */
	public double getLowVelocity() {
		return lowVelocity;
	}

	/**
	 * @return the highVelocoty
	 */
	public double getHighVelocity() {
		return highVelocity;
	}

	/**
	 * @return the restitution
	 */
	public double getRestitution() {
		return restitution;
	}

	/**
	 * @return the size
	 */
	public double getSize() {
		return size;
	}

	/**
	 * @return the xPos
	 */
	public double getxPos() {
		return xPos;
	}

	/**
	 * @return the yPos
	 */
	public double getyPos() {
		return yPos;
	}
	
	
}
