/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;
import com.github.wolfie.blackboard.annotation.ListenerMethod;

/**
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface FeatureListener extends Listener {
	
	/**
	 * This method is called when the current visible feature has been changed (visible)
	 * @param newEvent
	 */
	@ListenerMethod
	void featureChanged(FeatureEvent newEvent);
}
