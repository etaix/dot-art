/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Event;

/**
 * This event is fired when a feature is shown to the user. The event contains the title of the new feature and also its description
 * @author Eric Taix (eric.taix@gmail.com)
 */
public class FeatureEvent implements Event {

	private String title;
	private String description;
	
	public FeatureEvent(String titleP, String descriptionP) {
		title = titleP;
		description = descriptionP;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
}
