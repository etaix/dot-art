/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;

/**
 * A ColorListener is a class which is interested to any change of the countdown colors (dots, background)
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface ColorsListener extends Listener {

	/**
	 * The countdown's type has changed
	 * @param evt
	 */
	public void colorChanged(ColorsEvent  evtP);
}
