/**
 * 
 */
package com.jared.dotart.event;

import com.github.wolfie.blackboard.Listener;

/**
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface MenuListener extends Listener {

	/**
	 * The selected menu has changed
	 * @param event
	 */
	public void menuChanged(MenuEvent event);
}
