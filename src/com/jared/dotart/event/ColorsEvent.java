/**
 * 
 */
package com.jared.dotart.event;

import java.awt.image.BufferedImage;

import com.github.wolfie.blackboard.Event;

/**
 * This event is fired when a color changed (dots colors or background color/image/repeat)
 * @author Eric Taix (eric.taix@gmail.com)
 */
public class ColorsEvent implements Event {

	private String daysColor = "#265397";
	private String hoursColor = "#13acfa";
	private String minutesColor = "#c0000b";
	private String secondesColors = "#009a49";
	private String separatorsColor = "#b6b4b5";
	private String otherdotColor = "#c9c9c9";
	
	private String backgroundColor;
	private String backgroundName;
	private BufferedImage backgroundImage;
	private boolean repeatX = false;
	private boolean repeatY = false;
	
	public ColorsEvent() {
	}

	/**
	 * Set all dots color at the same time
	 * @param dc
	 * @param hc
	 * @param mc
	 * @param sc
	 * @param sepc
	 * @param oc
	 */
	public void setDotsColor(String dc, String hc, String mc, String sc, String sepc, String oc) {
		daysColor = dc;
		hoursColor = hc;
		minutesColor = mc;
		secondesColors = sc;
		separatorsColor = sepc;
		otherdotColor = oc;
	}
	
	/**
	 * Set background properties at the same time
	 * @param col
	 * @param img
	 * @param rx
	 * @param ry
	 */
	public void setBackground(String col, String imgn, BufferedImage img, boolean rx, boolean ry) {
		backgroundColor = col;
		backgroundImage = img;
		backgroundName = imgn;
		repeatX = rx;
		repeatY = ry;
	}

	/**
	 * @return the daysColor
	 */
	public String getDaysColor() {
		return daysColor;
	}

	/**
	 * @return the hoursColor
	 */
	public String getHoursColor() {
		return hoursColor;
	}

	/**
	 * @return the minutesColor
	 */
	public String getMinutesColor() {
		return minutesColor;
	}

	/**
	 * @return the secondesColors
	 */
	public String getSecondesColors() {
		return secondesColors;
	}

	/**
	 * @return the separatorsColor
	 */
	public String getSeparatorsColor() {
		return separatorsColor;
	}

	/**
	 * @return the otherdotColor
	 */
	public String getOtherdotColor() {
		return otherdotColor;
	}

	/**
	 * @return the backgroundColor
	 */
	public String getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @return the backgroundImage
	 */
	public BufferedImage getBackgroundImage() {
		return backgroundImage;
	}

	/**
	 * @return the repeatX
	 */
	public boolean isRepeatX() {
		return repeatX;
	}

	/**
	 * @return the repeatY
	 */
	public boolean isRepeatY() {
		return repeatY;
	}

	/**
	 * @return the backgroundName
	 */
	public String getBackgroundName() {
		return backgroundName;
	}
	
	/**
	 * Return colors in the following order: days, hours, minutes, secondes, separator, others
	 * @return
	 */
	public String[] getColors() {
		String[] result = new String[6];
		result[0] = daysColor;
		result[1] = hoursColor;
		result[2] = minutesColor;
		result[3] = secondesColors;
		result[4] = separatorsColor;
		result[5] = otherdotColor;
		return result;
	}
}
