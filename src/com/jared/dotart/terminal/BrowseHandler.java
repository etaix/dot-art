/**
 * 
 */
package com.jared.dotart.terminal;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipFile;

import com.jared.dotart.DotartApplication;
import com.vaadin.service.FileTypeResolver;
import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.URIHandler;

/**
 * Utility class to manage ZipResource
 * 
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class BrowseHandler implements URIHandler {

	private static final String COUNTDOWN_ARCHIVE = "countdown.zip";
	private static final String SCREENSHOT_FILENAME = "screenshot.png";
	private static final String BROWSE_DIR = "browse/";

	private static final Logger log = Logger.getLogger(ZipResource.class.getName());

	// The application
	private DotartApplication application;

	/**
	 * The constructor
	 * 
	 * @param zipFile
	 * @param app
	 */
	public BrowseHandler(DotartApplication app) {
		application = app;
	}

	public DownloadStream handleURI(URL context, String relativeUri) {
		if (log.isLoggable(Level.FINEST)) { 
			log.finest("Handling URL: " + relativeUri);
		}
		// The URI format is browse/<dir>/countdown.html OR browse/<dir>/countdown.zip
		// BUT don't handle URI if it requests the screenshot.png
		if (!relativeUri.startsWith(BROWSE_DIR) || relativeUri.endsWith(SCREENSHOT_FILENAME)) {
			return null;
		}
		// Retrieve the directory
		String subdir = relativeUri.substring(BROWSE_DIR.length());
		int index = subdir.indexOf("/");
		if (index != -1) {
			String dir = subdir.substring(0, index);
			String fileEntry = subdir.substring(index + 1);
			File file = new File(application.getContext().getBaseDirectory() + "/WEB-INF/" + BROWSE_DIR + dir + "/"+COUNTDOWN_ARCHIVE);
			try {
				// If we are requested the archive
				if (!fileEntry.equals(COUNTDOWN_ARCHIVE)) {
					// Get the archive and returns the entry
					ZipFile zipFile = new ZipFile(file);
					ZipResource res = new ZipResource(fileEntry, zipFile, application);
					return res.getStream();

				}
				// Just download the archive
				else {
					FileInputStream fis = new FileInputStream(file);
					DownloadStream ds = new DownloadStream(fis, FileTypeResolver.getMIMEType(COUNTDOWN_ARCHIVE), COUNTDOWN_ARCHIVE);;
					ds.setCacheTime(0);
					return ds;
				}
			} catch (Exception ex) {
				log.finest("Error while opening Zip: " + application.getContext().getBaseDirectory() + "/WEB-INF/" + BROWSE_DIR + dir
						+ "/" + COUNTDOWN_ARCHIVE);
			}
		}
		return null;
	}
}
