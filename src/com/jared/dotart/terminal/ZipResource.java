/**
 * 
 */
package com.jared.dotart.terminal;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.vaadin.Application;
import com.vaadin.service.FileTypeResolver;
import com.vaadin.terminal.ApplicationResource;
import com.vaadin.terminal.DownloadStream;

/**
 * A resource contained is a Zip file
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class ZipResource implements ApplicationResource {

	private static final Logger log = Logger.getLogger(ZipResource.class.getName());
	
	// The filename of the resource
	private String filename;
    // Default buffer size for this stream resource.
    private int bufferSize = 0;
    // Default cache time for this stream resource.
    private long cacheTime = DEFAULT_CACHETIME;
    // Zip file containing the file 
    private ZipFile zipFile;
    // Application used for serving the class.
    private final Application application;
	
    
    public ZipResource(String name, ZipFile zip, Application app) {
    	filename = name;
    	application = app;
    	zipFile = zip;
    }
    
	/* (non-Javadoc)
	 * @see com.vaadin.terminal.Resource#getMIMEType()
	 */
	public String getMIMEType() {
        return FileTypeResolver.getMIMEType(filename);
	}

	/* (non-Javadoc)
	 * @see com.vaadin.terminal.ApplicationResource#getStream()
	 */
	public DownloadStream getStream() {
		ZipEntry entry = zipFile.getEntry(filename);
		InputStream is;
		try {
			is = zipFile.getInputStream(entry);
			DownloadStream ds = new DownloadStream(is, getMIMEType(), getFilename());
			ds.setCacheTime(24*60*60*1000);
			return ds;
		} catch (IOException e) {
			log.severe("Error while retrieve zipentry stream for "+filename+": "+e.getMessage());
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.vaadin.terminal.ApplicationResource#getApplication()
	 */
	public Application getApplication() {
		return application;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.terminal.ApplicationResource#getFilename()
	 */
	public String getFilename() {
		return filename;
	}


	/* (non-Javadoc)
	 * @see com.vaadin.terminal.ApplicationResource#getBufferSize()
	 */
	public int getBufferSize() {
        return bufferSize;
    }

    /**
     * Sets the size of the download buffer used for this resource.
     * 
     * @param bufferSize
     *            the size of the buffer in bytes.
     */
    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }
    
	/* (non-Javadoc)
	 * @see com.vaadin.terminal.ApplicationResource#getCacheTime()
	 */
	public long getCacheTime() {
        return cacheTime;
	}
	
    /**
     * Sets the length of cache expiration time.
     * 
     * <p>
     * This gives the adapter the possibility cache streams sent to the client.
     * The caching may be made in adapter or at the client if the client
     * supports caching. Zero or negavive value disbales the caching of this
     * stream.
     * </p>
     * 
     * @param cacheTime
     *            the cache time in milliseconds.
     * 
     */
    public void setCacheTime(long cacheTime) {
        this.cacheTime = cacheTime;
    }

}
