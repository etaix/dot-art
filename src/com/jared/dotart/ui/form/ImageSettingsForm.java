/**
 * 
 */
package com.jared.dotart.ui.form;

import java.awt.image.BufferedImage;
import java.util.Arrays;

import org.vaadin.imagefilter.Image;

import com.jared.dotart.model.ImageSettings;
import com.vaadin.data.util.BeanItem;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Slider;
import com.vaadin.ui.AbsoluteLayout.ComponentPosition;

/**
 * The form to edit advanced settings
 * 
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class ImageSettingsForm extends Form {

	private static final int SIMULATOR_HEIGHT = 190;
	private static final int SIMULATOR_WIDTH = 300;
	private static int X_RESOLUTION = 1920;
	private static int Y_RESOLUTION = 1200;
	private GridLayout layout;
	private Image simImage;
	private AbsoluteLayout simuLayout;
	private Slider dotSize;
	private Slider dotSpacing;
	private Slider xPos;
	private Slider yPos;
	private int simulateWidth;
	private int simulateHeight;

	public ImageSettingsForm(BeanItem<ImageSettings> imgSettings) {
		// Create our layout (2x3)
		layout = new GridLayout(2, 5);
		layout.addStyleName("settings");
		layout.setWidth("100%");
		// Use top-left margin and spacing
		layout.setMargin(true, false, false, true);
		layout.setSpacing(true);

		setLayout(layout);

		// Set up buffering
		setWriteThrough(false);
		// No invalid values in datamodel
		setInvalidCommitted(false);

		// FieldFactory for customizing the fields and adding validators
		setFormFieldFactory(new ImageSettingsFieldFactory());
		// Bind to POJO via BeanItem
		setItemDataSource(imgSettings);

		// Determines which properties are shown, and in which order:
		setVisibleItemProperties(Arrays.asList(new String[] { "xCenter", "yCenter", "dotSize", "dotSpacing", "delay" }));
		
		// Add static 'fields'
		Label name = new Label(imgSettings.getItemProperty("name"));
		layout.addComponent(name,0,0,1,0);
		name.addStyleName("title");
		
		simuLayout = new AbsoluteLayout();
		simuLayout.setWidth("500px");
		simuLayout.setHeight("278px");
		layout.addComponent(simuLayout,0,1,1,1);
		Embedded monitor = new Embedded(null, new ThemeResource("img/monitor.jpg"));
		simuLayout.addComponent(monitor,"top:0px; left:85px");
		// Calculate simulation dimensions
		Image img = (Image)imgSettings.getItemProperty("image").getValue();
		BufferedImage originalImage = img.getOriginalImage();

		simulateWidth = (originalImage.getWidth() * SIMULATOR_WIDTH) / X_RESOLUTION;
		simulateHeight = (originalImage.getHeight() * SIMULATOR_HEIGHT) / Y_RESOLUTION;
		simImage = new Image(img.getInputStream(),true);
		simImage.setImmediate(true);
		simuLayout.addComponent(simImage);
		// Retrieve fields and add listener
		ValueChangeListener listener = new ValueChangeListener() {			
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				updateSimulation();
			}
		};
		dotSize = (Slider)getField("dotSize");
		dotSize.setImmediate(true);
		dotSize.addListener(listener);
		dotSpacing = (Slider)getField("dotSpacing");
		dotSpacing.setImmediate(true);
		dotSpacing.addListener(listener);		
		xPos = (Slider)getField("xCenter");
		xPos.setImmediate(true);
		xPos.addListener(listener);	
		yPos = (Slider)getField("yCenter");
		yPos.setImmediate(true);
		yPos.addListener(listener);	
		// Update the simulation
		updateSimulation();
	}

	/**
	 * Update the dimensions and position of the image
	 */
	private void updateSimulation() {
		int dsi = ((Double) dotSize.getValue()).intValue();
		int dsp = ((Double) dotSpacing.getValue()).intValue();
		int xp =  ((Double) xPos.getValue()).intValue();
		int yp =  ((Double) yPos.getValue()).intValue();
		// Calculate the dimensions according to the dot size and spacing
		int sW = (simulateWidth * dsi) + dsp;
		int sH = (simulateHeight * dsi) + dsp;
		simImage.setWidth(""+sW+"px");
		simImage.setHeight(""+sH+"px");
		int x = ((SIMULATOR_WIDTH) / 2)+98-(sW/2);
		int y = ((SIMULATOR_HEIGHT) / 2)+13-(sH/2);
		x = 94 + (SIMULATOR_WIDTH / 2) - (sW / 2);
		y = 4 + (SIMULATOR_HEIGHT / 2) - (sH / 2);
		x += (SIMULATOR_WIDTH * xp) / 100;
		y += (SIMULATOR_HEIGHT * yp) / 100;
		ComponentPosition pos = simuLayout.getPosition(simImage);
		pos.setCSSString("top:"+y+"px; left:"+x+"px");		
	}
	
	
	/*
	 * Override to get control over where fields are placed.
	 */
	@Override
	protected void attachField(Object propertyId, Field field) {
		if (propertyId.equals("xCenter")) {
			layout.addComponent(field, 0, 2);
		} else if (propertyId.equals("yCenter")) {
			layout.addComponent(field, 1, 2);
		} else if (propertyId.equals("dotSize")) {
			layout.addComponent(field, 0, 3);
		} else if (propertyId.equals("dotSpacing")) {
			layout.addComponent(field, 1, 3);
		} else if (propertyId.equals("delay")) {
			layout.addComponent(field, 0, 4);
		}
	}
}
