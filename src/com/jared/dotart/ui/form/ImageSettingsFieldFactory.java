/**
 * 
 */
package com.jared.dotart.ui.form;

import com.vaadin.data.Item;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Slider;

/**
 * Factory to create form fields
 * 
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class ImageSettingsFieldFactory extends DefaultFieldFactory {

	/**
	 * Default constructor
	 */
	public ImageSettingsFieldFactory() {
	}

	/**
	 * Creates and returns a field according to the propertyId
	 */
	@Override
	public Field createField(Item item, Object propertyId, Component uiContext) {
		Slider f = new Slider();
		f.setWidth("200px");
		if ("xCenter".equals(propertyId)) {
			f.setCaption("X pos of the image (% of the screen width)");
			f.setMin(-50);
			f.setMax(50);
		} else if ("yCenter".equals(propertyId)) {
			f.setCaption("Y pos of the image (% of the screen height)");
			f.setMin(-50);
			f.setMax(50);
		}
		if ("dotSize".equals(propertyId)) {
			f.setCaption("Dot size");
			f.setMin(1);
			f.setMax(60);
		}
		if ("dotSpacing".equals(propertyId)) {
			f.setCaption("Dot spacing");
			f.setMin(-10);
			f.setMax(10);
		}
		if ("delay".equals(propertyId)) {
			f.setCaption("Delay in seconds");
			f.setMin(1);
			f.setMax(30);
			f.setResolution(1);
		}
		return f;
	}
}