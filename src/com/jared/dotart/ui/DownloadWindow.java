/**
 * 
 */
package com.jared.dotart.ui;

import com.jared.dotart.GAUtils;
import com.vaadin.Application;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * This component provides a window which automatically download the zipped package and also provides textual information
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class DownloadWindow {

	private Application application;
	private Window subwindow;

	@SuppressWarnings("serial")
	public DownloadWindow(Application app) {
		application = app;

		// Create the window
		subwindow = new Window("Download an Art-Package");
		subwindow.setModal(true);

		// Configure the windws layout; by default a VerticalLayout
		VerticalLayout layout = (VerticalLayout) subwindow.getContent();
		layout.setMargin(true);
		layout.setSpacing(true);

		// Add some content; a label and a close-button
		Label message = new Label("Your download will start automatically in 5 seconds.<br/>" +
				"If it doesn't start, you can start it "+GAUtils.getExternalLinkWithGA("download", null, "/generator/download", "by downloading it manually")+".<br/><br/>" +
				"Once downloaded, just extract the package and launch the file 'countdown.html'<br/><br/>" +
				"Careful: If you have already extracted a package before, you may come accross a cache problem: you won't see what you generated.<br/>" +
				"It is a browser 'issue' and not a Dot-Art one. To resolve this issue, extract the package to another folder or clean your browser cache.", Label.CONTENT_XML);
		subwindow.addComponent(message);

		Button close = new Button("Close", new Button.ClickListener() {
			// inline click-listener
			public void buttonClick(ClickEvent event) {
				// close the window by removing it from the parent window
				application.getMainWindow().removeWindow(subwindow);
			}
		});
		// The components added to the window are actually added to the window's
		// layout; you can use either. Alignments are set using the layout
		layout.addComponent(close);
		layout.setComponentAlignment(close, Alignment.TOP_RIGHT);
		
		subwindow.setWidth("500px");
	}

	public void download(String urlP) {
		  if (subwindow.getParent() != null) {
              // window is already showing
              application.getMainWindow().showNotification(
                      "Window is already open");
          } else {
              // Open the subwindow by adding it to the parent
              // window
        	  application.getMainWindow().addWindow(subwindow);
          }
	}
	
}
