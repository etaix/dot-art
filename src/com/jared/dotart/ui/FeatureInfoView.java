/**
 * 
 */
package com.jared.dotart.ui;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.FeatureEvent;
import com.jared.dotart.event.FeatureListener;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;

/**
 * This panel shows informations about the current screen
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class FeatureInfoView extends Panel implements FeatureListener {
	
	// The property which contains data
	private ObjectProperty<String> property = new ObjectProperty<String>("");
	private Label description;
	
	public FeatureInfoView() {
        addStyleName("feature-info");
        setWidth("250px");
        setHeight("100%");
        setCaption("Tips and Tricks");
        description = new Label(property, Label.CONTENT_XHTML);
    	addComponent(description);
    	// Set default text
    	property.setValue("This window will show you essential tips");
    	
    	// Add myself as a listener (FeatureListener)
    	DotartApplication.getBlackboard().addListener(this);
	}
	

	/* (non-Javadoc)
	 * @see com.jared.dotart.event.FeatureListener#featureChanged(com.jared.dotart.event.FeatureEvent)
	 */
	public void featureChanged(FeatureEvent newFeature) {
		property.setValue(newFeature.getDescription());
	}
	
}
