/**
 * 
 */
package com.jared.dotart.ui.tab;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import com.jared.dotart.ui.Featured;
import com.jared.dotart.ui.component.UploadedTable;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;

/**
 * This tab contains components to upload and manage images orders
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class ImageTab extends VerticalLayout implements Featured {

	private ImageReceiver receiver = new ImageReceiver();
	private UploadedTable imageTable;
	private Upload upload;

	/**
	 * The constructor
	 */
	public ImageTab() {
		setHeight("100%");
		setWidth("95%");
		addStyleName("container");
		addStyleName("timeline_tab");
		setMargin(true, true, true, true);
		setSpacing(true);

		VerticalLayout layout = new VerticalLayout();
		layout.setSpacing(true);
		layout.setSizeFull();
		addComponent(layout);

		// Create the upload component
		upload = new Upload(null, receiver);
		upload.setCaption(null);
		upload.setImmediate(true);
		upload.setButtonCaption("Add an image");
		upload.addListener(new Upload.StartedListener() {
			public void uploadStarted(StartedEvent event) {
				// Verify if the filename extension is suitable for dotart
				String filename = event.getFilename();
				if (filename.matches(".*\\.(jpg|jpeg|gif|png)")) {
					imageTable.uploadImage(event.getFilename());
				}
				else {
					upload.interruptUpload();
					getWindow().showNotification("Not supported", "You can only upload an image with the following format: GIF, PNG, JPG, JPEG", Notification.TYPE_WARNING_MESSAGE);
				}
			}
		});
		layout.addComponent(upload);
		
		// Create the upload table
		imageTable = new UploadedTable(upload);
		layout.addComponent(imageTable);
		layout.setExpandRatio(imageTable, 1.0f);
	}



	// ================ Inner Classes ==============
	
	/* (non-Javadoc)
	 * @see com.jared.dotart.ui.Featured#getFeatureDesc()
	 */
	public String getFeatureDesc() {
		return "Dot-Art supports the following image formats: GIF, PNG, JPG, JPEG.<br/><br/>"
				+ "When using JPG or JPEG, there's no alpha channel.<br/><br/>"
				+ "The chronological order of the images and the countdown, can be changed by dragging rows (drag a row by clicking in the first column). "
				+ "Images which are before the countdown will be always displayed. Images after the countdown, will be displayed after the countdown has reached zero.<br/><br/>"
				+ "You can change advanced image settings, by clickink the 'Settings' button in the image row.<br/><br/>"
				+ "The recommended maximum size is 120px width and 120px height. Of course you can add a bigger image, but the final result may be very slow (depending on your computer).";
	}


	/**
	 * Inner class which receives the image stream
	 */
	public static class ImageReceiver implements Receiver {

		private String fileName;
		private String mtype;
		private ByteArrayOutputStream os;

		public OutputStream receiveUpload(String filename, String mimetype) {
			fileName = filename;
			mtype = mimetype;
			os = new ByteArrayOutputStream();
			return os;
		}

		public String getFileName() {
			return fileName;
		}

		public String getMimeType() {
			return mtype;
		}
		
		public ByteArrayOutputStream getStream() {
			return os;
		}
	}


}
