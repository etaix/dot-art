/**
 * 
 */
package com.jared.dotart.ui.tab;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.MiscellaneousEvent;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.Runo;

/**
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class TitlePanel extends CustomComponent {

	private TextField title;
	private TextField watchAgainCaption;
	private TextField finalMsg;
	private TextField finalURL;

	public TitlePanel() {
		setWidth("100%");
		// ============ Add Date panel ===========
		Panel miscellaneousPanel = new Panel("Miscellaneous");
		miscellaneousPanel.setStyleName(Runo.PANEL_LIGHT);
		GridLayout miscellaneousLayout = new GridLayout(2, 3);
		miscellaneousPanel.setWidth("100%");
		miscellaneousPanel.setContent(miscellaneousLayout);
		miscellaneousLayout.setWidth("100%");
		miscellaneousLayout.setColumnExpandRatio(1, 1.0f);
		miscellaneousLayout.setMargin(true);
		miscellaneousLayout.setSpacing(true);
		miscellaneousLayout.setColumnExpandRatio(0, 1.0f);
		miscellaneousLayout.setColumnExpandRatio(1, 1.0f);

		// Create a common listener
		ValueChangeListener changeListener = new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				fireMiscellaneaousEvent();		
			}
		};
		
		title = new TextField("Window title", "Dot-Art experience");
		title.setImmediate(true);
		miscellaneousLayout.addComponent(title);
		title.setWidth("300px");
		title.addListener(changeListener);
		
		watchAgainCaption = new TextField("Watch again button title","Watch again");
		miscellaneousLayout.addComponent(watchAgainCaption,1,0);
		watchAgainCaption.setWidth("300px");
		watchAgainCaption.addListener(changeListener);
		
		finalMsg = new TextField("Final message","");
		miscellaneousLayout.addComponent(finalMsg,0,1);
		finalMsg.setWidth("300px");
		finalMsg.addListener(changeListener);
		
		finalURL = new TextField("Final URL","");
		miscellaneousLayout.addComponent(finalURL,1,1);
		finalURL.setWidth("300px");
		finalURL.addListener(changeListener);
		
		// Type of countdown
		setCompositionRoot(miscellaneousPanel);
	}
	
	/**
	 * Fire a {@link MiscellaneousEvent} event
	 */
	private void fireMiscellaneaousEvent() {
		MiscellaneousEvent evt = new MiscellaneousEvent();
		evt.setTitle((String)title.getValue());
		evt.setWatchAgainTitle((String)watchAgainCaption.getValue());
		evt.setFinalMessage((String)finalMsg.getValue());
		evt.setFinalUrl((String)finalURL.getValue());
		DotartApplication.getBlackboard().fire(evt);
	}
}
