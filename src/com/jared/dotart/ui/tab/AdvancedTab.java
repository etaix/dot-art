/**
 * 
 */
package com.jared.dotart.ui.tab;

import com.jared.dotart.ui.Featured;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * This tab displays principal options of the countdown
 * 
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class AdvancedTab extends Panel implements Featured {

	public AdvancedTab() {
		setHeight("100%");
		setWidth("99%");
		setStyleName(Runo.PANEL_LIGHT);
		addStyleName("advanced_tab");
		addStyleName("container");
		VerticalLayout mainLayout = new VerticalLayout();
		setContent(mainLayout);
		mainLayout.setWidth("99%");
		mainLayout.addStyleName("container");
		mainLayout.setMargin(true, true, true, true);
		mainLayout.setSpacing(false);

		// ============ Add Miscellaneaous panel ===========
		mainLayout.addComponent(new CountdownSettingsPanel());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.ui.Featured#getFeatureDesc()
	 */
	public String getFeatureDesc() {
		return "If you want to create a countdown for an event purpose (such as a JUG event), use an absolute countdown and set the event's date.<br/><br/>"
				+ "If it is for a demo purpose, prefer to create a relative countdown : your users will see a fresh countdown each time they refresh the page.<br/><br/>"
				+ "The chronological order of the countdown can be set in the tab image.<br/><br/>" +
				"Play with different settings to learn their respective effect.<br/><br/>" +
				"When setting the X and Y position, you must take into account the resolution used to display the countdown.";
	}

}
