/**
 * 
 */
package com.jared.dotart.ui.tab;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.vaadin.imagefilter.FilterOperation;
import org.vaadin.imagefilter.Image;
import org.vaadin.imagefilter.filters.FitIntoFilter;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.ColorsEvent;
import com.jared.dotart.ui.Featured;
import com.jared.dotart.ui.component.CountdownLabel;
import com.jared.dotart.ui.tab.ImageTab.ImageReceiver;
import com.vaadin.addon.colorpicker.ColorPicker;
import com.vaadin.addon.colorpicker.ColorPicker.ColorChangeListener;
import com.vaadin.addon.colorpicker.events.ColorChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Runo;

/**
 * This tab displays styles informations
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class ColorsTab extends Panel implements Featured {

	private static final String BUTTON_WIDTH = "130px";
	private static final String LABEL_WIDTH = "80px";
	private Image backgroundImage;
	private String backgroundName;
	private CountdownLabel example;
	private ColorPicker backgroundColorPicker;
	private CheckBox cbRepeatX;
	private CheckBox cbRepeatY;

	public ColorsTab() {
		setHeight("100%");
		setWidth("99%");
		setStyleName(Runo.PANEL_LIGHT);
		addStyleName("styles_tab");
		VerticalLayout mainLayout = new VerticalLayout();
		setContent(mainLayout);
		mainLayout.setWidth("99%");

		mainLayout.addStyleName("container");
		mainLayout.setMargin(true, true, true, true);
		mainLayout.setSpacing(true);

		example = new CountdownLabel();

		// Add colors panel
		Panel countdownPanel = new Panel("Dots colors");
		mainLayout.addComponent(countdownPanel);
		countdownPanel.setStyleName(Runo.PANEL_LIGHT);
		countdownPanel.setWidth("100%");
		GridLayout colorsLayout = new GridLayout(3, 6);
		colorsLayout.setWidth("100%");
		colorsLayout.setMargin(true);
		colorsLayout.setSpacing(true);
		colorsLayout.setColumnExpandRatio(2, 1.0f);
		countdownPanel.setContent(colorsLayout);

		Label daysLabel = new Label("Days");
		daysLabel.setWidth(LABEL_WIDTH);
		colorsLayout.addComponent(daysLabel, 0, 0);
		ColorPicker daysPicker = new ColorPicker("Days color", Color.decode("#265897"));
		colorsLayout.addComponent(daysPicker, 1, 0);
		daysPicker.setWidth(BUTTON_WIDTH);
		daysPicker.setHistoryVisibility(false);
		daysPicker.setImmediate(true);
		daysPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				example.setDaysColor(event.getColor());
				fireColorsEvent();
			}
		});

		Label hoursLabel = new Label("Hours");
		colorsLayout.addComponent(hoursLabel, 0, 1);
		ColorPicker hoursPicker = new ColorPicker("Hours color", Color.decode("#13acfa"));
		colorsLayout.addComponent(hoursPicker, 1, 1);
		hoursPicker.setWidth(BUTTON_WIDTH);
		hoursPicker.setHistoryVisibility(false);
		hoursPicker.setImmediate(true);
		hoursPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				example.setHoursColor(event.getColor());
				fireColorsEvent();
			}
		});

		Label minLabel = new Label("Minutes");
		colorsLayout.addComponent(minLabel, 0, 2);
		ColorPicker minPicker = new ColorPicker("Minutes color", Color.decode("#c0000b"));
		colorsLayout.addComponent(minPicker, 1, 2);
		minPicker.setWidth(BUTTON_WIDTH);
		minPicker.setHistoryVisibility(false);
		minPicker.setImmediate(true);
		minPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				example.setMinutesColor(event.getColor());
				fireColorsEvent();
			}
		});

		Label secLabel = new Label("Secondes");
		colorsLayout.addComponent(secLabel, 0, 3);
		ColorPicker secPicker = new ColorPicker("Secondes color", Color.decode("#009a49"));
		colorsLayout.addComponent(secPicker, 1, 3);
		secPicker.setWidth(BUTTON_WIDTH);
		secPicker.setHistoryVisibility(false);
		secPicker.setImmediate(true);
		secPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				example.setSecondesColors(event.getColor());
				fireColorsEvent();
			}
		});

		Label sepLabel = new Label("Separator");
		colorsLayout.addComponent(sepLabel, 0, 4);
		ColorPicker sepPicker = new ColorPicker("Separator color", Color.decode("#b6b4b5"));
		colorsLayout.addComponent(sepPicker, 1, 4);
		sepPicker.setWidth(BUTTON_WIDTH);
		sepPicker.setHistoryVisibility(false);
		sepPicker.setImmediate(true);
		sepPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				example.setSeparatorsColor(event.getColor());
				fireColorsEvent();
			}
		});

		Label otherLabel = new Label("Others");
		colorsLayout.addComponent(otherLabel, 0, 5);
		ColorPicker otherPicker = new ColorPicker("Others", Color.decode("#c9c9c9"));
		colorsLayout.addComponent(otherPicker, 1, 5);
		otherPicker.setWidth(BUTTON_WIDTH);
		otherPicker.setHistoryVisibility(false);
		otherPicker.setImmediate(true);
		otherPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				example.setOtherdotColor(event.getColor());
				fireColorsEvent();
			}
		});

		HorizontalLayout wrapExample = new HorizontalLayout();
		wrapExample.setSizeFull();
		colorsLayout.addComponent(wrapExample, 2, 0, 2, 5);
		wrapExample.addComponent(example);
		wrapExample.setComponentAlignment(example, Alignment.MIDDLE_CENTER);

		// ============= Add colors panel ==============
		Panel backgroundPanel = new Panel("Background");
		mainLayout.addComponent(backgroundPanel);
		backgroundPanel.setStyleName(Runo.PANEL_LIGHT);
		backgroundPanel.setWidth("100%");
		final GridLayout backgroundLayout = new GridLayout(4, 6);
		backgroundPanel.setContent(backgroundLayout);
		backgroundLayout.setMargin(true);
		backgroundLayout.setSpacing(true);
		backgroundLayout.setWidth("100%");
		backgroundLayout.setColumnExpandRatio(3, 1.0f);

		Label backColorLabel = new Label("Color");
		backColorLabel.setWidth(LABEL_WIDTH);
		backgroundLayout.addComponent(backColorLabel, 0, 0);
		backgroundColorPicker = new ColorPicker("Background color", Color.decode("#ffffff"));
		backgroundLayout.addComponent(backgroundColorPicker, 1, 0);
		backgroundColorPicker.setWidth(BUTTON_WIDTH);
		backgroundColorPicker.setHistoryVisibility(false);
		backgroundColorPicker.setImmediate(true);
		backgroundColorPicker.addListener(new ColorChangeListener() {
			public void colorChanged(ColorChangeEvent event) {
				fireColorsEvent();
			}
		});
		// The background upload
		Label backImage = new Label("Image");
		backgroundLayout.addComponent(backImage, 0, 1);
		final ProgressIndicator pi = new ProgressIndicator();
		ImageReceiver receiver = new ImageReceiver();
		final Upload upload = new Upload(null, receiver);
		backgroundLayout.addComponent(upload, 1, 1);
		upload.setImmediate(true);
		upload.setButtonCaption("Select an image");

		final HorizontalLayout progressLayout = new HorizontalLayout();
		backgroundLayout.addComponent(progressLayout, 2, 1);
		progressLayout.setSpacing(true);
		progressLayout.setVisible(false);
		progressLayout.addComponent(pi);
		progressLayout.setComponentAlignment(pi, Alignment.MIDDLE_LEFT);

		final Button cancelProcessing = new Button("Cancel");
		cancelProcessing.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				upload.interruptUpload();
			}
		});
		cancelProcessing.setStyleName("small");
		progressLayout.addComponent(cancelProcessing);
		backgroundImage = new Image(true);
		backgroundImage.addStyleName("bg-image");
		final HorizontalLayout imgLayout = new HorizontalLayout();
		backgroundLayout.addComponent(imgLayout, 3, 0, 3, 5);
		backgroundLayout.setComponentAlignment(imgLayout, Alignment.MIDDLE_CENTER);
		imgLayout.addComponent(backgroundImage);
		final Button removeButton = new Button("Clear");
		removeButton.setStyleName(BaseTheme.BUTTON_LINK);
		removeButton.addStyleName("small-link");
		imgLayout.addComponent(removeButton);
		removeButton.setVisible(false);
		removeButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				Image newImage = new Image(true);
				newImage.addStyleName("bg-image");
				imgLayout.replaceComponent(backgroundImage, newImage);
				backgroundImage = newImage;
				backgroundImage.setVisible(false);
				backgroundName = null;
				removeButton.setVisible(false);
				backgroundLayout.requestRepaint();
				fireColorsEvent();
			}
		});

		Label repeatXLabel = new Label("Repeat-X");
		backgroundLayout.addComponent(repeatXLabel, 0, 3);
		cbRepeatX = new CheckBox();
		cbRepeatX.setImmediate(true);
		cbRepeatX.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				fireColorsEvent();
			}
		});
		backgroundLayout.addComponent(cbRepeatX, 1, 3);
		Label repeatYLabel = new Label("Repeat-Y");
		backgroundLayout.addComponent(repeatYLabel, 0, 4);
		cbRepeatY = new CheckBox();
		cbRepeatY.setImmediate(true);
		cbRepeatY.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				fireColorsEvent();
			}
		});
		backgroundLayout.addComponent(cbRepeatY, 1, 4);

		/*
		 * =========== Add needed listener for the upload component: start, progress, finish, success, fail ===========
		 */
		// Needed listener for the upload component
		upload.addListener(new Upload.StartedListener() {
			public void uploadStarted(StartedEvent event) {
				progressLayout.setVisible(true);
				pi.setValue(0f);
				pi.setPollingInterval(500);
			}
		});

		upload.addListener(new Upload.ProgressListener() {
			public void updateProgress(long readBytes, long contentLength) {
				pi.setValue(new Float(readBytes / (float) contentLength));
			}

		});

		upload.addListener(new Upload.SucceededListener() {
			public void uploadSucceeded(SucceededEvent event) {
				getWindow().showNotification("Image " + event.getFilename() + " uploaded", Notification.TYPE_TRAY_NOTIFICATION);
				final ByteArrayOutputStream os = ((ImageReceiver) upload.getReceiver()).getStream();
				ByteArrayInputStream bis = new ByteArrayInputStream(os.toByteArray());
				backgroundImage.load(bis);
				// Fit the image to a maximum size
				FilterOperation op = FilterOperation.getByName(FilterOperation.FITINTO);
				((FitIntoFilter) op.getFilter()).setWidth(250);
				((FitIntoFilter) op.getFilter()).setHeight(250);
				backgroundImage.addOperation(op);
				backgroundImage.applyOperations();
				backgroundImage.setVisible(true);
				backgroundName = event.getFilename();
				fireColorsEvent();
			}
		});

		upload.addListener(new Upload.FailedListener() {
			public void uploadFailed(FailedEvent event) {
				getWindow().showNotification("Image " + event.getFilename() + " failed", Notification.TYPE_TRAY_NOTIFICATION);
			}
		});

		upload.addListener(new Upload.FinishedListener() {
			public void uploadFinished(FinishedEvent event) {
				progressLayout.setVisible(false);
				upload.setVisible(true);
				removeButton.setVisible(true);
			}
		});

	}

	/**
	 * Retrieve all colors and fire a color event to update the genertor
	 */
	private void fireColorsEvent() {
		ColorsEvent evt = new ColorsEvent();
		evt.setDotsColor(example.getDaysColor(), example.getHoursColor(), example.getMinutesColor(), example.getSecondesColors(), example.getSeparatorsColor(),
				example.getOtherdotColor());
		BufferedImage bi = null;
		if (backgroundImage != null) {
			if (backgroundImage.getOriginalImage() != null) {
				bi = backgroundImage.getOriginalImage();
			}
		}
		evt.setBackground(CountdownLabel.encodeColor(backgroundColorPicker.getColor()), backgroundName, bi, cbRepeatX.booleanValue(), cbRepeatY.booleanValue());
		DotartApplication.getBlackboard().fire(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.ui.Featured#getFeatureDesc()
	 */
	public String getFeatureDesc() {
		return "This page lets you define colors for the countdown and the background.<br/><br/>"
				+ "The background is the same for the countdown and for images animation. There's no way to set a different background.<br/><br/>"
				+ "Background styles are quite simple, but if you have a basic skill in HTML and CSS, you will be able to modify the generated files to fit your needs.<br/><br/>"
				+ "<b>Careful:</b> If you modify the HTML file, please do not remove any <code>&lt;div&gt;</code> tags.";
	}
}
