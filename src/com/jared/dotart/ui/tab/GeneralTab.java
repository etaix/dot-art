/**
 * 
 */
package com.jared.dotart.ui.tab;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.jared.dotart.Countdown.Type;
import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.CountdownTypeEvent;
import com.jared.dotart.ui.Featured;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.InlineDateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Select;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * This tab displays principal options of the countdown
 * 
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class GeneralTab extends Panel implements Featured {

	private static final List<String> COUNTDOWN_TYPES = Arrays.asList(new String[] { "An absolute countdown", "A relative countdown", "No countdown, thanks" });

	// The current visible content
	private Component dateComponent;
	// The component to edit an absolute date
	private AbstractComponent absComponent;
	// The component for no aountdown
	private AbstractComponent noneComponent = new VerticalLayout();
	// Component to edit a relative time offset
	private AbstractComponent relComponent;
	// The layout which contains the date fields
	private VerticalLayout dateLayout;
	// The absolute date
	private PopupDateField absoluteDate;
	// The relative hours
	private NativeSelect hours;
	// The relative minutes
	private NativeSelect minutes;
	// The relative seconds
	private NativeSelect seconds;

	private OptionGroup countDownType;

	public GeneralTab() {
		setHeight("100%");
		setWidth("99%");
		setStyleName(Runo.PANEL_LIGHT);
		addStyleName("general_tab");
		addStyleName("container");
		VerticalLayout mainLayout = new VerticalLayout();
		setContent(mainLayout);
		mainLayout.setWidth("99%");
		mainLayout.addStyleName("container");
		mainLayout.setMargin(true, true, true, true);
		mainLayout.setSpacing(false);

		// ============ Add Miscellaneaous panel ===========
		mainLayout.addComponent(new TitlePanel());
		
		
		// ============ Add Date panel ===========
		Panel datePanel = new Panel("Date");
		mainLayout.addComponent(datePanel);
		datePanel.setStyleName(Runo.PANEL_LIGHT);
		GridLayout datePanelLayout = new GridLayout(2, 1);
		datePanel.setWidth("100%");
		datePanel.setContent(datePanelLayout);
		datePanelLayout.setWidth("100%");
		datePanelLayout.setColumnExpandRatio(1, 1.0f);
		datePanelLayout.setMargin(true);
		datePanelLayout.setSpacing(true);

		// Type of countdown
		AbstractComponent kindComponent = createKindGroup();
		kindComponent.setWidth("350px");
		datePanelLayout.addComponent(kindComponent, 0, 0);
		dateLayout = new VerticalLayout();
		datePanelLayout.addComponent(dateLayout, 1, 0);
		absComponent = createAbsoluteDateGroup();
		relComponent = createRelativeDateGroup();
		dateComponent = relComponent;
		dateLayout.addComponent(dateComponent);

	
	}

	/**
	 * Creates a group which displays the countdown type options
	 */
	private AbstractComponent createKindGroup() {
		VerticalLayout layout = new VerticalLayout();
		Label typeLabel = new Label("What kind of countdown would you like to display ? ");
		// typeLabel.setStyleName(Runo.LABEL_H2);
		layout.addComponent(typeLabel);

		countDownType = new OptionGroup();
		countDownType.setItemCaptionMode(Select.ITEM_CAPTION_MODE_EXPLICIT);
		for (int iLoop = 0; iLoop < COUNTDOWN_TYPES.size(); iLoop++) {
			countDownType.addItem(iLoop);
			countDownType.setItemCaption(iLoop, COUNTDOWN_TYPES.get(iLoop));
		}
		countDownType.setNullSelectionAllowed(false);
		countDownType.select(1);
		countDownType.setVisible(true);
		countDownType.setImmediate(true);
		// Add a listener to show / hide options according to the current selected item
		countDownType.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				Property prop = event.getProperty();
				int index = (Integer) prop.getValue();
				switch (index) {
				case 0:
					dateLayout.replaceComponent(dateComponent, absComponent);
					dateComponent = absComponent;
					break;
				case 1:
					dateLayout.replaceComponent(dateComponent, relComponent);
					dateComponent = relComponent;
					break;
				case 2:
					dateLayout.replaceComponent(dateComponent, noneComponent);
					dateComponent = noneComponent;
					break;
				}
				dateLayout.setExpandRatio(dateComponent, 1.0f);
				fireCountdownEvent();
			}
		});
		layout.addComponent(countDownType);
		return layout;
	}

	private void fireCountdownEvent() {
		Type type = Type.values()[(Integer)countDownType.getValue()];
		// Notify that Countdown's type has changed
		int sec = ((Integer)hours.getValue())*3600+((Integer)minutes.getValue())*60+((Integer)seconds.getValue());
		DotartApplication.getBlackboard().fire(new CountdownTypeEvent(type, (Date)absoluteDate.getValue(), sec));		
	}
	
	/**
	 * Creates a group which displays the absolute date selection
	 * 
	 * @return
	 */
	private AbstractComponent createAbsoluteDateGroup() {
		VerticalLayout absLayout = new VerticalLayout();
		absLayout.setWidth("300px");
		absLayout.setMargin(true);
		absLayout.addStyleName("absolute-date");
		Label absLabel = new Label("Set the absolute date and time");
		absLayout.addComponent(absLabel);
		absoluteDate = new PopupDateField();
		absoluteDate.setValue(new java.util.Date(System.currentTimeMillis()+(60*60*1000)));
		absoluteDate.setResolution(InlineDateField.RESOLUTION_MIN);
		absoluteDate.setImmediate(true);
		absoluteDate.setVisible(true);
		absLayout.addComponent(absoluteDate);
		absLayout.setVisible(true);
		absoluteDate.addListener(new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				fireCountdownEvent();				
			}
		});
		return absLayout;
	}

	/**
	 * Creates a group which displays the relative date selection
	 * 
	 * @return
	 */
	private AbstractComponent createRelativeDateGroup() {
		VerticalLayout relLayout = new VerticalLayout();
		relLayout.setMargin(true);
		relLayout.addStyleName("relative-date");
		Label relLabel = new Label("Set the time offset (HH:MM:SS)");
		relLayout.addComponent(relLabel);
		HorizontalLayout fieldLayout = new HorizontalLayout();
		relLayout.addComponent(fieldLayout);
		hours = new NativeSelect();
		hours.setImmediate(true);
		fieldLayout.addComponent(hours);
		hours.setNullSelectionAllowed(false);
		for (int i = 0; i <= 23; i++) {
			hours.addItem(i);
		}
		fieldLayout.addComponent(new Label(":"));
		minutes = new NativeSelect();
		minutes.setImmediate(true);
		fieldLayout.addComponent(minutes);
		minutes.setNullSelectionAllowed(false);
		for (int i = 0; i <= 59; i++) {
			minutes.addItem(i);
		}
		fieldLayout.addComponent(new Label(":"));
		seconds = new NativeSelect();
		seconds.setImmediate(true);
		fieldLayout.addComponent(seconds);
		seconds.setNullSelectionAllowed(false);
		for (int i = 0; i <= 59; i++) {
			seconds.addItem(i);
		}
		// Set the default offset to 5 secondes
		hours.select(0);
		minutes.select(0);
		seconds.select(15);
		relLayout.setVisible(true);
		// Add listeners
		ValueChangeListener fire = new ValueChangeListener() {
			public void valueChange(ValueChangeEvent event) {
				fireCountdownEvent();
			}
		};
		hours.addListener(fire);
		minutes.addListener(fire);
		seconds.addListener(fire);
		return relLayout;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.ui.Featured#getFeatureDesc()
	 */
	public String getFeatureDesc() {
		return "The 'Watch Button' let the user to watch your art again. If the title is empty, then the button is hidden.<br/><br/>"
				+ "The final message is displayed when the animation is finished. This is typically used to let the user visit your web site. If you leave it empty, no message will be displayed. "
				+ "Also if you set a final URL then when the user click on the final message, he will be redirect to this URL.<br/><br/>"	
	            + "If you want to create a countdown for an event purpose (such as a JUG event), use an absolute countdown and set the event's date.<br/><br/>"
				+ "If it is for a demo purpose, prefer to create a relative countdown : your users will see a fresh countdown each time they refresh the page.<br/><br/>"
				+ "The chronological order of the countdown can be set in the tab image.<br/><br/>";
	}

}
