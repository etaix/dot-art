/**
 * 
 */
package com.jared.dotart.ui.tab;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.SettingsEvent;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.Slider.ValueOutOfBoundsException;
import com.vaadin.ui.themes.Runo;

/**
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class CountdownSettingsPanel extends CustomComponent {

	public CountdownSettingsPanel() {
		setWidth("100%");

		// ============ Add Setting panel ===========
		Panel settingsPanel = new Panel("Countdown");
		settingsPanel.setWidth("100%");
		settingsPanel.setStyleName(Runo.PANEL_LIGHT);
		GridLayout settingsPanelLayout = new GridLayout(2, 4);
		settingsPanelLayout.addStyleName("grid-settings");
		settingsPanelLayout.setMargin(true);
		settingsPanelLayout.setSpacing(true);
		settingsPanelLayout.setWidth("100%");
		settingsPanel.setContent(settingsPanelLayout);

		try {
			final Slider xGravity = new Slider("Gravity on the X axis (between -500 and +500)");
			xGravity.setWidth("300px");
			xGravity.setMin(-500);
			xGravity.setMax(500);
			xGravity.setImmediate(true);
			settingsPanelLayout.addComponent(xGravity, 0, 0);

			final Slider yGravity = new Slider("Gravity on the Y axis (between -500 and +500)");
			yGravity.setWidth("300px");
			yGravity.setMin(-500);
			yGravity.setMax(500);
			yGravity.setImmediate(true);

			yGravity.setValue(400.0d);
			settingsPanelLayout.addComponent(yGravity, 1, 0);

			final Slider lowVelocity = new Slider("Low velocity (between 0 and 1000)");
			lowVelocity.setWidth("300px");
			lowVelocity.setMin(0);
			lowVelocity.setMax(1000);
			lowVelocity.setImmediate(true);

			lowVelocity.setValue(300.0d);
			settingsPanelLayout.addComponent(lowVelocity, 0, 1);

			final Slider highVelocity = new Slider("High velocity (between 0 and 1000)");
			highVelocity.setWidth("300px");
			highVelocity.setMin(0);
			highVelocity.setMax(1000);
			highVelocity.setImmediate(true);
			highVelocity.setValue(1000.0d);
			settingsPanelLayout.addComponent(highVelocity, 1, 1);

			final Slider restitutionVelocity = new Slider("Dots restitution (between 0 and 12)");
			restitutionVelocity.setWidth("300px");
			restitutionVelocity.setMin(0);
			restitutionVelocity.setMax(12);
			highVelocity.setImmediate(true);
			restitutionVelocity.setValue(5d);
			settingsPanelLayout.addComponent(restitutionVelocity, 0, 2);

			final Slider dotsSize = new Slider("Dot's size (between 5 and 30)");
			dotsSize.setWidth("300px");
			dotsSize.setMin(5);
			dotsSize.setMax(30);
			dotsSize.setImmediate(true);
			dotsSize.setValue(18);
			settingsPanelLayout.addComponent(dotsSize, 1, 2);

			final Slider xPos = new Slider("X position (between 0 and 3000)");
			xPos.setWidth("300px");
			xPos.setMin(0);
			xPos.setMax(3000);
			xPos.setImmediate(true);
			xPos.setValue(495);
			settingsPanelLayout.addComponent(xPos, 0, 3);

			final Slider yPos = new Slider("Y position (between 0 and 3000)");
			yPos.setWidth("300px");
			yPos.setMin(0);
			yPos.setMax(3000);
			yPos.setImmediate(true);
			yPos.setValue(307);
			settingsPanelLayout.addComponent(yPos, 1, 3);

			// Add the listener
			ValueChangeListener settingsListener = new ValueChangeListener() {
				public void valueChange(ValueChangeEvent event) {
					SettingsEvent evt = new SettingsEvent();
					evt.setSettings((Double) xGravity.getValue(), (Double) yGravity.getValue(), (Double) lowVelocity.getValue(), (Double) highVelocity.getValue(),
							(Double) restitutionVelocity.getValue(), (Double) dotsSize.getValue(), (Double) xPos.getValue(), (Double) yPos.getValue());
					DotartApplication.getBlackboard().fire(evt);
				}
			};
			xGravity.addListener(settingsListener);
			yGravity.addListener(settingsListener);
			highVelocity.addListener(settingsListener);
			lowVelocity.addListener(settingsListener);
			restitutionVelocity.addListener(settingsListener);
			dotsSize.addListener(settingsListener);
			xPos.addListener(settingsListener);
			yPos.addListener(settingsListener);

		}
		catch (ValueOutOfBoundsException e) {
			e.printStackTrace();
		}
		
		// Type of countdown
		setCompositionRoot(settingsPanel);
	}
}
