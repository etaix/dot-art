/**
 * 
 */
package com.jared.dotart.ui;

import org.vaadin.dialogs.ConfirmDialog;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.GAUtils;
import com.jared.dotart.event.FeatureEvent;
import com.jared.dotart.ui.tab.AdvancedTab;
import com.jared.dotart.ui.tab.ColorsTab;
import com.jared.dotart.ui.tab.GeneralTab;
import com.jared.dotart.ui.tab.ImageTab;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.SelectedTabChangeListener;
import com.vaadin.ui.VerticalLayout;

/**
 * This TabSheet is the main component of a dotart creation.
 * 
 * @author Eric Taix
 */
public class CreatePage extends VerticalLayout {

	private static final long serialVersionUID = 1L;
	private DownloadWindow downloadWindow;

	@SuppressWarnings("serial")
	public CreatePage() {
		setWidth("1100px");
		setHeight("100%");

		FeatureInfoView infoView = new FeatureInfoView();
		HorizontalLayout createContent = new HorizontalLayout();

		addComponent(createContent);
		setExpandRatio(createContent, 1.0f);
		createContent.setSizeFull();
		createContent.setSpacing(true);
		createContent.setMargin(false, true, false, false);

		// Add a blank view
		VerticalLayout leftBlankLayout = new VerticalLayout();
		createContent.addComponent(leftBlankLayout);
		createContent.setExpandRatio(leftBlankLayout, 0.2f);
		// Add the tabsheet
		TabSheet tabSheet = new TabSheet();
		tabSheet.setWidth("800px");
		tabSheet.setHeight("100%");
		createContent.addComponent(tabSheet);
		createContent.setExpandRatio(tabSheet, 1.0f);

		// Add the feature view
		createContent.addComponent(infoView);
		// Add a blank view
		VerticalLayout rightBlankLayout = new VerticalLayout();
		createContent.addComponent(rightBlankLayout);
		createContent.setExpandRatio(rightBlankLayout, 0.2f);

		// ===== Tabsheet definition =====

		// Countdown definition
		GeneralTab countDownLayout = new GeneralTab();
		tabSheet.addComponent(countDownLayout);
		tabSheet.getTab(countDownLayout).setCaption("General");

		// Images definition
		ImageTab imageTab = new ImageTab();
		tabSheet.addComponent(imageTab);
		tabSheet.getTab(imageTab).setCaption("Images");

		// Styles definition
		ColorsTab stylesLayout = new ColorsTab();
		tabSheet.addComponent(stylesLayout);
		tabSheet.getTab(stylesLayout).setCaption("Styles");

		// Miscellaneaous definition
		AdvancedTab advanced = new AdvancedTab();
		tabSheet.addComponent(advanced);
		tabSheet.getTab(advanced).setCaption("Advanced settings");

		// Add a listener to be aware of selected tab changes
		tabSheet.addListener(new SelectedTabChangeListener() {
			public void selectedTabChange(SelectedTabChangeEvent event) {
				if (event.getTabSheet() != null) {
					if (event.getTabSheet().getSelectedTab() != null) {
						Component comp = event.getTabSheet().getSelectedTab();
						if (comp instanceof Featured) {
							FeatureEvent evt = new FeatureEvent("",
									((Featured) comp).getFeatureDesc());
							DotartApplication.getBlackboard().fire(evt);
							return;
						}
					}
				}
				// If no feature has been found, set a default message
				FeatureEvent evt = new FeatureEvent("",
						"No description available");
				DotartApplication.getBlackboard().fire(evt);
			}
		});

		// Add the buttons layout
		HorizontalLayout wrapper = new HorizontalLayout();
		wrapper.setWidth("800px");
		addComponent(wrapper);
		setComponentAlignment(wrapper, Alignment.MIDDLE_LEFT);

		HorizontalLayout buttonsLayout = new HorizontalLayout();
		wrapper.addComponent(buttonsLayout);
		wrapper.setComponentAlignment(buttonsLayout, Alignment.MIDDLE_CENTER);
		buttonsLayout.setSpacing(false);
		buttonsLayout.setMargin(true);
		buttonsLayout.setWidth("400px");

		Button resetButton = new Button("Reset");
		buttonsLayout.addComponent(resetButton);
		buttonsLayout.setComponentAlignment(resetButton, Alignment.MIDDLE_LEFT);
		resetButton.setWidth("100px");
		resetButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				// The quickest way to confirm
				ConfirmDialog confirm = ConfirmDialog
						.show(getApplication().getMainWindow(),
								"Please confirm",
								"Your data will be loose. Would you like to continue ?",
								"Yes please", "No",
								new ConfirmDialog.Listener() {
									public void onClose(ConfirmDialog dialog) {
										if (dialog.isConfirmed()) {
											getApplication().close();
										}
									}
								});
				confirm.setHeight("200px");
			}
		});

		ExternalResource er = new ExternalResource("generator/countdown.html");
		String caption = GAUtils
				.getExternalLinkWithGA(er.getURL(),
						"preview",
						"/generator/preview",
						// The following lines are copy pasted from rendered
						// Vaadin v6.1 buttons.
						"<div class='v-button' tabindex='0'><span class='v-button-wrap'><span class='v-button-caption'>Preview</span></span></div></a>");
		Label label = new Label(caption, Label.CONTENT_XHTML);
		buttonsLayout.addComponent(label);
		buttonsLayout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		label.setWidth("100px");

		Button downloadButton = new Button("Generate");
		buttonsLayout.addComponent(downloadButton);
		buttonsLayout.setComponentAlignment(downloadButton,
				Alignment.MIDDLE_RIGHT);
		downloadButton.setWidth("100px");
		downloadButton.addListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				downloadWindow.download("toto");
			}
		});

		// Set if the first featured informations (if available)
		FeatureEvent evt = new FeatureEvent("",
				countDownLayout.getFeatureDesc());
		DotartApplication.getBlackboard().fire(evt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.AbstractComponentContainer#attach()
	 */
	@Override
	public void attach() {
		super.attach();
		downloadWindow = new DownloadWindow(getApplication());
	}

}
