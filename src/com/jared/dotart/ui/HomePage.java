/**
 * 
 */
package com.jared.dotart.ui;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.themes.Runo;

/**
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class HomePage extends CustomComponent {

	/**
	 * The constructor initialize the UI
	 */
	public HomePage() {
		setWidth("1200px");
		setHeight("100%");
		// Create the content
		Panel content = new Panel();
		content.setStyleName(Runo.PANEL_LIGHT);
		content.setSizeFull();

		GridLayout layout = new GridLayout(3, 4);
		layout.setMargin(false);
		layout.setSpacing(true);
		layout.setWidth("100%");
		layout.setHeight("100%");
		layout.setColumnExpandRatio(0, 1.0f);
		layout.setRowExpandRatio(1, 1.0f);

		// The text
		Label homeLabel = new Label("Do you want to create your own countdown,<br/><br/><br/>for a special event, the new year or just for fun ?", Label.CONTENT_XHTML);
		homeLabel.setSizeUndefined();
		homeLabel.setStyleName("home1");
		layout.addComponent(homeLabel, 0, 0, 2, 0);
		layout.setComponentAlignment(homeLabel, Alignment.TOP_CENTER);
		// Adver for Devoxx
		Link e = new Link();
		e.setResource(new ExternalResource("http://www.devoxx.com/display/FR12/Accueil"));
		e.setIcon(new ThemeResource("./img/devoxxfr.jpg"));
		e.addStyleName("pub");
		layout.addComponent(e, 2, 1, 2, 2);
		layout.setComponentAlignment(e, Alignment.MIDDLE_RIGHT);
		// The screencast
		Embedded sc = new Embedded(null, new ExternalResource("http://www.youtube.com/v/V5-6z_pQ4gY?version=3&feature=player_detailpage"));
		sc.addStyleName("screencast");
		sc.setMimeType("application/x-shockwave-flash");
		sc.setParameter("allowFullScreen", "true");
		sc.setParameter("allowScriptAccess", "always");
		sc.setWidth("520px");
		sc.setHeight("345px");
		layout.addComponent(sc, 1, 1);
		layout.setComponentAlignment(sc, Alignment.MIDDLE_RIGHT);

		// Browser tips
		Label browserLabel = new Label("Watch the original <a href='http://www.weareinstrument.com/countdown/'>Google's 2011 developer conference countdown</a><br/>"
				+ "For a better user experience, use a fast and modern browser such as Chrome<br/>"
				+ "DotArt is free for Java User Group, non profit organizations and any other organizations. If you are a profit organization, feel free to donate ;-)",
				Label.CONTENT_XHTML);
		browserLabel.addStyleName("home2");
		browserLabel.setSizeUndefined();
		layout.addComponent(browserLabel, 0, 3, 2, 3);
		layout.setComponentAlignment(browserLabel, Alignment.MIDDLE_CENTER);
		// News panel
		Panel news = new Panel("News");
		news.setStyleName(Runo.PANEL_LIGHT);
		news.addStyleName("news");
		news.setWidth("200px");
		news.setHeight("100%");
		layout.addComponent(news, 0, 1);
		Label newsLabel = new Label(getNews(), Label.CONTENT_XHTML);
		news.addComponent(newsLabel);
		content.addComponent(layout);
		setCompositionRoot(content);

	}

	private String getNews() {
		return "" 
 		        + "<b>2012/01/05</b> Pre-alpha 3 released: Now support different alpha channel on PNG files.<br/><br/>"
		 		+ "<b>2011/12/20</b> Pre-alpha 2 released: Image advanced settings editor improved. Reset button available.<br/><br/>"
				+ "<b>2011/12/19</b> Pre-alpha 1 released: All major features are implemented<br/>";
	}
}
