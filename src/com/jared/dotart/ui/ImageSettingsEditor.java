/**
 * 
 */
package com.jared.dotart.ui;

import com.jared.dotart.model.ImageSettings;
import com.jared.dotart.ui.form.ImageSettingsForm;
import com.vaadin.Application;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;


/**
 * Image advanced setting editor
 * @author Eric Taix (eric dot taix at gmail dot com 
 */
public class ImageSettingsEditor {
	private Application application;
	private Window subwindow;
	private VerticalLayout layout;

	/**
	 * Default constructor
	 */
	public ImageSettingsEditor(Application app) {	
		application = app;

		// Create the window
		subwindow = new Window("Image advanced settings");
		subwindow.setModal(true);
		subwindow.setWidth("600px");
		layout = (VerticalLayout) subwindow.getContent();
	}
	

	@SuppressWarnings("serial")
	public void edit(ImageSettings settings) {
		// Create the form
		BeanItem<ImageSettings> bean = new BeanItem<ImageSettings>(settings);
		final ImageSettingsForm form = new ImageSettingsForm(bean);
		
		  // The cancel / apply buttons
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.setSpacing(true);
        Button apply = new Button("Apply", new ClickListener() {
            public void buttonClick(ClickEvent event) {
                try {
                	form.commit();
    				// Close the window by removing it from the parent window
    				application.getMainWindow().removeWindow(subwindow);                    	
                } catch (Exception e) {
                }
            }
        });
        buttons.addComponent(apply);
        buttons.setComponentAlignment(apply, Alignment.MIDDLE_RIGHT);
        Button discardChanges = new Button("Discard", new ClickListener() {
                    public void buttonClick(ClickEvent event) {
                    	form.discard();
        				// Close the window by removing it from the parent window
        				application.getMainWindow().removeWindow(subwindow);                    	
                    }
                });
        buttons.addComponent(discardChanges);
        buttons.setComponentAlignment(discardChanges, Alignment.MIDDLE_RIGHT);

        HorizontalLayout footer = (HorizontalLayout)form.getFooter();
        footer.setWidth("100%");
        footer.setMargin(true);
        footer.addComponent(buttons);
        footer.setComponentAlignment(buttons, Alignment.MIDDLE_RIGHT);
        
		// Add the form to the layout
		layout.removeAllComponents();
		layout.addComponent(form);
		  if (subwindow.getParent() != null) {
              // window is already showing
              application.getMainWindow().showNotification(
                      "Window is already open");
          } else {
              // Open the subwindow by adding it to the parent window
        	  application.getMainWindow().addWindow(subwindow);
          }
	}
	
}
