/**
 * 
 */
package com.jared.dotart.ui;

/**
 * A feature is just an interface which can provide its own description
 * @author Eric Taix (eric.taix@gmail.com)
 */
public interface Featured {

	/**
	 * Returns its description
	 * @return
	 */
	public String getFeatureDesc();
}
