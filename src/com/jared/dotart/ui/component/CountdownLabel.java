/**
 * 
 */
package com.jared.dotart.ui.component;

import java.awt.Color;

import com.vaadin.data.util.ObjectProperty;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;

/**
 * This is special label which display a countdown with appropriate colors
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class CountdownLabel extends CustomComponent {

	// The property which contains data
	private ObjectProperty<String> property = new ObjectProperty<String>("");
	private String daysColor = "#265897";
	private String hoursColor = "#13acfa";
	private String minutesColor = "#c0000b";
	private String secondesColors = "#009a49";
	private String separatorsColor = "#b6b4b5";
	private String otherdotColor = "#c9c9c9";
	
	public CountdownLabel() {
		Label label = new Label(property, Label.CONTENT_XHTML);	
		addStyleName("countdown-example");
		setHeight("80px");
		setCompositionRoot(label);
		updateColors();
	}
	
	/**
	 * Update the label datasource according to colors
	 */
	private void updateColors() {
		String value = createFontTag("089", daysColor) +
				createFontTag(":", separatorsColor) +
				createFontTag("10", hoursColor) +
				createFontTag(":", separatorsColor) +
				createFontTag("20", minutesColor) +
				createFontTag(":", separatorsColor) +
				createFontTag("34", secondesColors);
		property.setValue(value);
	}

	/**
	 * Creates an HTML font tag with the color parameter
	 * @param title
	 * @param color
	 * @return
	 */
	private String createFontTag(String title, String color) {
		return "<font color='"+color+"'>"+title+"</font>";
	}
	
	
	/**
	 * Encode a color into a String with the following format #RRGGBB
	 * @param color
	 * @return
	 */
	public static String encodeColor(Color color) {
        String red = Integer.toHexString(color.getRed());
        red = red.length() < 2 ? "0" + red : red;

        String green = Integer.toHexString(color.getGreen());
        green = green.length() < 2 ? "0" + green : green;

        String blue = Integer.toHexString(color.getBlue());
        blue = blue.length() < 2 ? "0" + blue : blue;

        return "#" + red + green + blue;		
	}
	
	/**
	 * @param daysColor the daysColor to set
	 */
	public void setDaysColor(Color daysColor) {
		this.daysColor = encodeColor(daysColor);
		updateColors();
	}

	/**
	 * @param hoursColor the hoursColor to set
	 */
	public void setHoursColor(Color hoursColor) {
		this.hoursColor = encodeColor(hoursColor);
		updateColors();
	}

	/**
	 * @param minColor the minColor to set
	 */
	public void setMinutesColor(Color minColor) {
		this.minutesColor = encodeColor(minColor);
		updateColors();
	}

	/**
	 * @param secondesColors the secondesColors to set
	 */
	public void setSecondesColors(Color secondesColors) {
		this.secondesColors = encodeColor(secondesColors);
		updateColors();
	}

	/**
	 * @param separatorsColor the separatorsColor to set
	 */
	public void setSeparatorsColor(Color separatorsColor) {
		this.separatorsColor = encodeColor(separatorsColor);
		updateColors();
	}

	/**
	 * @param otherdotColor the otherdotColor to set
	 */
	public void setOtherdotColor(Color otherdotColor) {
		this.otherdotColor = encodeColor(otherdotColor);
		updateColors();
	}

	/**
	 * @return the daysColor
	 */
	public String getDaysColor() {
		return daysColor;
	}

	/**
	 * @return the hoursColor
	 */
	public String getHoursColor() {
		return hoursColor;
	}

	/**
	 * @return the minutesColor
	 */
	public String getMinutesColor() {
		return minutesColor;
	}

	/**
	 * @return the secondesColors
	 */
	public String getSecondesColors() {
		return secondesColors;
	}

	/**
	 * @return the separatorsColor
	 */
	public String getSeparatorsColor() {
		return separatorsColor;
	}

	/**
	 * @return the otherdotColor
	 */
	public String getOtherdotColor() {
		return otherdotColor;
	}
	
}
