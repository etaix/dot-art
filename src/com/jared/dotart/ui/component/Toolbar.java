/**
 * 
 */
package com.jared.dotart.ui.component;

import java.util.ArrayList;
import java.util.List;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.MenuEvent;
import com.jared.dotart.event.MenuListener;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * This custom component is a toolbar with a logo, an image and a menu
 * 
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class Toolbar extends CustomComponent implements MenuListener {

	private List<Button> buttons = new ArrayList<Button>();

	/**
	 * Create the toolbar
	 * 
	 * @param themeImg
	 *            This is a relative image path (the image MUST be contained in the theme folder)
	 */
	public Toolbar(String themeImg, String[] menuTitles, String[] fragments) {
		HorizontalLayout nav = new HorizontalLayout();
		nav.setHeight("130px");
		nav.setWidth("100%");
		nav.addStyleName("topbar");
		nav.setSpacing(true);
		nav.setMargin(false, true, false, false);

		// Add the logo
		Embedded embeded = new Embedded("", new ThemeResource(themeImg));
		embeded.addStyleName("logo");
		nav.addComponent(embeded);
		nav.setExpandRatio(embeded, 1.0f);

		// Add menus
		for (int iLoop = 0; iLoop < menuTitles.length; iLoop++) {
			String menu = menuTitles[iLoop];
			final String fragment = fragments[iLoop];
			Button btn = new Button(menu);
			btn.setData(fragments[iLoop]);
			if (iLoop != 0) {
				btn.setStyleName(Runo.BUTTON_LINK + " " + "menu" + iLoop);
			} else {
				btn.setStyleName(Runo.BUTTON_LINK + " " + "menu" + iLoop + "-selected");
			}
			buttons.add(btn);
			nav.addComponent(btn);
			nav.setComponentAlignment(btn, Alignment.MIDDLE_RIGHT);
			// Add the listener
			btn.addListener(new ClickListener() {
				public void buttonClick(ClickEvent event) {
					// Fire menu event
					MenuEvent menuEvt = new MenuEvent(fragment);
					DotartApplication.getBlackboard().fire(menuEvt);
				}
			});
		}
		// Add myself as a listener (FeatureListener)
		DotartApplication.getBlackboard().addListener(this);

		setCompositionRoot(nav);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.MenuListener#menuChanged(com.jared.dotart.event.MenuEvent)
	 */
	public void menuChanged(MenuEvent event) {
		setMenu(event.getFeature());
	}
	
	/**
	 * Set the current selected menu
	 * @param menuId
	 */
	public void setMenu(String featureP) {
		// Remove all previous selected menu
		for (int iLoop = 0; iLoop < buttons.size(); iLoop++) {
			if (!((String)buttons.get(iLoop).getData()).equals(featureP)) {
				buttons.get(iLoop).setStyleName(Runo.BUTTON_LINK + " " + "menu" + iLoop);
			} else {
				buttons.get(iLoop).setStyleName(Runo.BUTTON_LINK + " " + "menu" + iLoop + "-selected");
			}
		}
	}

}
