/**
 * 
 */
package com.jared.dotart.ui.component;

import java.io.File;
import java.io.FileFilter;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;

import com.vaadin.service.ApplicationContext;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.themes.Runo;

/**
 * This component displays arts in a grid (2 columns width)
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class ArtDirectory extends CustomComponent {

	// The arts main content
	private Panel content;
	// The DOM parser
	private Builder parser;
	private GridLayout currentGrid;

	/**
	 * The constructor initialize the UI and also loads the browse directory to add the ArtItems
	 */
	public ArtDirectory() {
		parser = new Builder();
		// Create the content
		content = new Panel("Arts directory");
		Label infoDir = new Label("Did you create a cool art ? Do you want to see it in this directory ?" +
				" Just send an email to <a href='mailto:dotart.info@gmail.com'>dotart.info@gmail.com</a> with your name," +
				" your email (optionnal), a title, a description and of course your art package", Label.CONTENT_XHTML);
		content.addComponent(infoDir);
		content.setStyleName(Runo.PANEL_LIGHT);
		content.addStyleName("directory");
		content.setSizeFull();
		setWidth("1000px");
		setHeight("100%");
		currentGrid = new GridLayout();
		setCompositionRoot(content);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.AbstractComponentContainer#attach()
	 */
	@Override
	public void attach() {
		// Load the directory
		loadDirectory();
	}

	/**
	 * Browse the "browse" directory, and loads / creates artItem
	 */
	private void loadDirectory() {
		ApplicationContext ctx = getApplication().getContext();
		File root = ctx.getBaseDirectory();
		File browseDir = new File(root, "WEB-INF/browse");
		// If the browse dir exist
		if (browseDir != null && browseDir.exists()) {
			// List art directories
			File[] artDirectories = browseDir.listFiles(new FileFilter() {
				// To be an art directory
				// - It must be a directory (well hum...)
				// - It must contains the file dot-at.xml which describes the art
				public boolean accept(File file) {
					if (!file.isDirectory())
						return false;
					File dotArtXml = new File(file, "dot-art.xml");
					return dotArtXml.exists();
				}
			});
			// Creates the GridLayout according to the number of arts
			if (artDirectories.length > 0) {
				int col = 2;
				int row = artDirectories.length / 2;
				// If length is odd then add a row for the last art
				if (artDirectories.length % 2 == 1) {
					row++;
				}
				GridLayout gridLayout = new GridLayout(col, row);
				content.replaceComponent(currentGrid,gridLayout);
				currentGrid = gridLayout;
				gridLayout.setWidth("100%");
				gridLayout.setMargin(true);
				// For each directory, creates a ArtItem and add it
				for (File dir : artDirectories) {
					ArtItem item = createArtItem(dir);
					gridLayout.addComponent(item);
					gridLayout.setComponentAlignment(item, Alignment.MIDDLE_CENTER);
				}
			}

		}
	}

	/**
	 * Create an ArtItem. This method reads the dot-art.xml description file and creates the ArtItem according to readed informations
	 * 
	 * @param file
	 * @return
	 */
	private ArtItem createArtItem(File directoryRoot) {
		ArtItem item = null;
		File artFile = new File(directoryRoot, "dot-art.xml");
		File screenShot = new File(directoryRoot, "screenshot.png");
		try {
			// Retrieve Art informations
			Document doc = parser.build(artFile);
			Element root = doc.getRootElement();
			String title = root.getAttributeValue("title");
			Element author = root.getFirstChildElement("author");
			String description = root.getValue();
			// Create the art item
			item = new ArtItem(directoryRoot, title, author.getAttributeValue("name"), author.getAttributeValue("email"), description, screenShot);
		} catch (Exception ex) {
			System.err.println("Error while building file " + artFile.getAbsolutePath() + ": " + ex);
		}
		return item;
	}
}
