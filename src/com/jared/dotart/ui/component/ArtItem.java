package com.jared.dotart.ui.component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.vaadin.imagefilter.FilterOperation;
import org.vaadin.imagefilter.Image;
import org.vaadin.imagefilter.filters.FitIntoFilter;
import org.vaadin.imagefilter.filters.RoundedCornersFilter;

import com.vaadin.terminal.ExternalResource;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Runo;

@SuppressWarnings("serial")
public class ArtItem extends CssLayout {

	/**
	 * The constructor which initialize the title
	 */
	public ArtItem(File rootdir, String titleP, String authorName, String authorEmail, String description, File screenshot) {
		setMargin(true);
		addStyleName(Runo.CSSLAYOUT_SHADOW);
		GridLayout mainLayout = new GridLayout(2, 4);
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);
		mainLayout.addStyleName(Runo.LAYOUT_DARKER);
		mainLayout.setWidth("400px");
		mainLayout.setHeight("300px");
		addComponent(mainLayout);
		// Set values
		Label title = new Label(titleP);
		title.addStyleName("title");
		mainLayout.addComponent(title, 0, 0, 1, 0);
		Label desc = new Label(description);
		desc.addStyleName("description");
		mainLayout.addComponent(desc, 0, 1, 1, 1);
		Label author = new Label("by <a href='mailto:" + authorEmail + "'>" + authorName + "</a>", Label.CONTENT_XHTML);
		mainLayout.addComponent(author, 0, 3);
		HorizontalLayout actionLayout = new HorizontalLayout();
		mainLayout.addComponent(actionLayout, 1, 2);
		mainLayout.setComponentAlignment(actionLayout, Alignment.TOP_RIGHT);
		actionLayout.setSpacing(true);
		// Add buttons ONLY if the archive exists
		File archive = new File(rootdir, "countdown.zip");
		if (archive.exists()) {	
			Link previewBtn = new Link();
			actionLayout.addComponent(previewBtn);
			previewBtn.setStyleName(BaseTheme.BUTTON_LINK);
			previewBtn.setDescription("Preview the countdown");
			previewBtn.setIcon(new ThemeResource("img/preview.png"));
			previewBtn.setResource(new ExternalResource("browse/" + rootdir.getName() + "/countdown.html"));
			previewBtn.setTargetName("preview");
			Link downloadBtn = new Link();
			actionLayout.addComponent(downloadBtn);
			downloadBtn.setStyleName(BaseTheme.BUTTON_LINK);
			downloadBtn.setDescription("Download the countdown");
			downloadBtn.setIcon(new ThemeResource("img/download.png"));
			downloadBtn.setResource(new ExternalResource("browse/" + rootdir.getName() + "/countdown.zip"));
		}
		// Try to get the screenshot
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(screenshot);
			Image img = new Image(fis, true);
			// Fit image into a 100x100 box
			FilterOperation op = FilterOperation.getByName(FilterOperation.FITINTO);
			FitIntoFilter fif = (FitIntoFilter) op.getFilter();
			fif.setHeight(150);
			fif.setWidth(200);
			img.addOperation(op);
			// Round corners
			op = FilterOperation.getByName(FilterOperation.ROUNDEDCORNERS);
			RoundedCornersFilter rcf = (RoundedCornersFilter) op.getFilter();
			rcf.setCornerRadius(9);
			img.addOperation(op);
			img.applyOperations();
			mainLayout.addComponent(img, 0, 2, 0, 2);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
