/**
 * 
 */
package com.jared.dotart.ui.component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Collection;

import org.vaadin.imagefilter.Image;

import com.jared.dotart.Countdown.Type;
import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.CountdownListener;
import com.jared.dotart.event.CountdownTypeEvent;
import com.jared.dotart.event.ImageEvent;
import com.jared.dotart.model.ImageSettings;
import com.jared.dotart.ui.ImageSettingsEditor;
import com.jared.dotart.ui.tab.ImageTab.ImageReceiver;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.AbstractSelect.AbstractSelectTargetDetails;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FinishedEvent;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window.Notification;

/**
 * This custom component has a Table in which you can add image to upload
 * 
 * @author Eric Taix (eric.taix@gmail.com)
 */
@SuppressWarnings("serial")
public class UploadedTable extends CustomComponent implements Upload.ProgressListener, Upload.SucceededListener, Upload.FailedListener, Upload.FinishedListener, CountdownListener {

	public static final String COUNTDOWN_ID = "countdown_id";
	private Table table;
	private Upload upload;
	private UploadedImage uploadedImage;
	private ImageSettingsEditor editor;

	/**
	 * UploadTable constructor. Initialize the UI
	 * 
	 * @param uploadP
	 */
	public UploadedTable(Upload uploadP) {
		setSizeFull();

		upload = uploadP;
		table = new Table();
		table.setCacheRate(0);
		table.setImmediate(true);
		table.setPageLength(0);
		table.setSizeFull();
		table.addStyleName("components-inside");
		// Define the names and data types of columns.
		table.addContainerProperty("move", Embedded.class, null);
		table.addContainerProperty("Image", CustomComponent.class, null);
		table.setColumnWidth("move", 30);
		table.setColumnExpandRatio("Image", 1.0f);
		table.setColumnHeader("move", "");
		table.setColumnHeader("Image", "Timeline");
		// Set table options
		table.setColumnReorderingAllowed(false);
		table.setColumnCollapsingAllowed(false);
		table.setSortDisabled(true);

		initDragAndDrop();

		// Add myself as a listener (FeatureListener)
		DotartApplication.getBlackboard().addListener(this);

		setCompositionRoot(table);
		// By default the countdown is visible (relative)
		setCountDownVisible(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.ui.AbstractComponentContainer#attach()
	 */
	@Override
	public void attach() {
		super.attach();
		editor = new ImageSettingsEditor(getApplication());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.CountdownListener#countdownChanged(com.jared.dotart.event.CountdownEvent)
	 */
	public void countdownTypeChanged(CountdownTypeEvent evtP) {
		setCountDownVisible(evtP.getType() != Type.NONE);
	}

	/**
	 * Set if the countdown item (line) is visible or nt
	 */
	public void setCountDownVisible(boolean visibleP) {
		Item item = table.getItem(COUNTDOWN_ID);
		// Show the countdown
		if (visibleP) {
			// The countdown does not exist
			if (item == null) {
				CountdownImage cdImage = new CountdownImage();
				item = table.addItem(COUNTDOWN_ID);
				item.getItemProperty("Image").setValue(cdImage);
				Embedded embeded = new Embedded(null, new ThemeResource("img/up-down.png"));
				embeded.setDescription("Drag and drop a row to reorder the timeline");
				item.getItemProperty("move").setValue(embeded);
			}
		}
		// Hide the countdown
		else {
			// The countdown exists
			if (item != null) {
				table.removeItem(COUNTDOWN_ID);
			}
		}
		fireImagesChanged();
	}

	/**
	 * Initialize the drag and drop
	 */
	private void initDragAndDrop() {
		table.setDragMode(TableDragMode.ROW);
		table.setDropHandler(new DropHandler() {

			public AcceptCriterion getAcceptCriterion() {
				return AcceptAll.get();
			}

			public void drop(DragAndDropEvent dropEvent) {
				// criteria verify that this is safe
				DataBoundTransferable t = (DataBoundTransferable) dropEvent.getTransferable();
				// Make sure the drag source is the same table
				if (t.getSourceComponent() != table || !(t instanceof DataBoundTransferable)) {
					return;
				}
				// Get the source
				Container sourceContainer = t.getSourceContainer();
				Object sourceItemId = t.getItemId();
				Item sourceItem = sourceContainer.getItem(sourceItemId);

				// Retrieve the target
				AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails) dropEvent.getTargetDetails());
				Object targetItemId = dropData.getItemIdOver();

				// First get components and THEN remove the source (Container does not support items with the same IDs)
				CustomComponent image = (CustomComponent) sourceItem.getItemProperty("Image").getValue();
				table.removeItem(sourceItemId);

				// Create a new Item at the correct position
				if (targetItemId != null) {
					switch (dropData.getDropLocation()) {
					case BOTTOM:
						table.addItemAfter(targetItemId, sourceItemId);
						break;
					case MIDDLE:
					case TOP:
						Object prevItemId = table.prevItemId(targetItemId);
						table.addItemAfter(prevItemId, sourceItemId);
						break;
					}
				}

				// Set properties of the new item
				addItem(image, sourceItemId);
				// Update the image list
				fireImagesChanged();
			}
		});
	}

	/**
	 * Change item properties or add a new item is it does not exist
	 * 
	 * @param itemId
	 *            The item's id
	 * @param name
	 *            The displayed label
	 * @param img
	 *            The image to be displayed
	 * @param status
	 *            A horizontal layout which contains the current status of the item
	 * @param action
	 *            A horizontal layout which contains the available actions
	 * @return
	 */
	private Item addItem(CustomComponent uploadedImg, final Object itemId) {
		// Get the idem
		Item item = table.getItem(itemId);
		// If it does not exist, create it
		if (item == null) {
			item = table.addItem(itemId);
		}
		item.getItemProperty("Image").setValue(uploadedImg);
		Embedded embeded = new Embedded(null, new ThemeResource("img/up-down.png"));
		embeded.setDescription("Drag and drop a row to reorder the timeline");
		item.getItemProperty("move").setValue(embeded);
		if (uploadedImg instanceof UploadedImage) {
			// Set the itemid
			((UploadedImage)uploadedImg).setItemId(itemId);
			// Add the listener
			((UploadedImage)uploadedImg).setListener(new UploadImageListener() {
				public void removeClicked() {
					table.removeItem(itemId);
					fireImagesChanged();
				}
			});
		}
		return item;
	}

	/**
	 * Upload a new image
	 * 
	 * @param fileNameP
	 * @param uploadP
	 */
	public void uploadImage(final String fileNameP) {
		// Register listeners
		upload.addListener((Upload.ProgressListener) this);
		upload.addListener((Upload.FailedListener) this);
		upload.addListener((Upload.FinishedListener) this);
		upload.addListener((Upload.SucceededListener) this);

		// Create the item id now
		final Object itemId = table.addItem();
		// Create the embeded image
		uploadedImage = new UploadedImage(fileNameP, editor);

		// Add the item into the table
		addItem(uploadedImage, itemId);
	}

	/**
	 * This method gets called several times during the update
	 * 
	 * @param readBytes
	 * @param contentLength
	 */
	public void updateProgress(long readBytes, long contentLength) {
	}

	/**
	 * This method gets called when the upload finished successfully
	 * 
	 * @param event
	 */
	public void uploadSucceeded(SucceededEvent event) {
		getWindow().showNotification("Image " + event.getFilename() + " uploaded", Notification.TYPE_TRAY_NOTIFICATION);
		final ByteArrayOutputStream os = ((ImageReceiver) upload.getReceiver()).getStream();
		ByteArrayInputStream bis = new ByteArrayInputStream(os.toByteArray());

		ImageSettings settings = new ImageSettings();
		settings.setName(event.getFilename());
		settings.setSize(event.getLength());
		settings.setImage(new Image(bis, true));
		uploadedImage.set(settings);

		fireImagesChanged();
	}

	/**
	 * Fire the new image list
	 */
	@SuppressWarnings("unchecked")
	private void fireImagesChanged() {
		// Retrieve all ids
		Container container = table.getContainerDataSource();
		Collection<Object> ids = (Collection<Object>) container.getItemIds();

		// For all items
		ImageEvent evt = new ImageEvent();
		for (Object id : ids) {
			Item item = container.getItem(id);
			TimelineItem image = (TimelineItem) item.getItemProperty("Image").getValue();
			ImageSettings settings = image.getSettings();
			// Add the ImageItem
			evt.addItem(settings);
			// Exclude the countdown image
			// if (id != COUNTDOWN_ID) {
			// Image img = ((Image) item.getItemProperty("Image").getValue());
			// imageItem.image = img.getImage();
			// }
			// evt.addItem(imageItem);
		}
		// Generated an image event
		DotartApplication.getBlackboard().fire(evt);
	}

	/**
	 * This method gets called when the upload failed
	 * 
	 * @param event
	 */
	public void uploadFailed(FailedEvent event) {
		getWindow().showNotification("Image " + event.getFilename() + " failed", Notification.TYPE_TRAY_NOTIFICATION);
	}

	/**
	 * This method gets called always when the upload finished, either succeeding or failing
	 * 
	 * @param event
	 */
	public void uploadFinished(FinishedEvent event) {
		upload.setVisible(true);

		// Unregister listeners
		upload.removeListener((Upload.ProgressListener) this);
		upload.removeListener((Upload.FailedListener) this);
		upload.removeListener((Upload.FinishedListener) this);
		upload.removeListener((Upload.SucceededListener) this);
	}
}
