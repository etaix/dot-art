/**
 * 
 */
package com.jared.dotart.ui.component;

/**
 * Defines methods called by the uploadImage itself but which will be managed by another component
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public interface UploadImageListener {

	/**
	 * The user has clicked on the remove button
	 */
	public void removeClicked();
	
}
