/**
 * 
 */
package com.jared.dotart.ui.component;

import com.jared.dotart.model.ImageSettings;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.VerticalLayout;

/**
 * This component aims to show the image, its name, its size and aims to provide component to set dot's size and space between dots
 * 
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class CountdownImage extends CustomComponent implements TimelineItem {

	private ImageSettings settings;
	
	public CountdownImage() {

		setSizeFull();
		setHeight("70px");
		addStyleName("uploaded-image");

		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		
		Embedded img = new Embedded(null, new ThemeResource("img/countdown.png"));
		layout.addComponent(img);
		
		setCompositionRoot(layout);
		
		// Init the imageSettings
		settings = new ImageSettings();
		settings.setItemId(UploadedTable.COUNTDOWN_ID);
	}
		
	/**
	 * Return the current image settings
	 * @return
	 */
	public ImageSettings getSettings() {
		return settings;
	}


	/**
	 * @return the itemId
	 */
	public Object getItemId() {
		return UploadedTable.COUNTDOWN_ID;
	}
}
