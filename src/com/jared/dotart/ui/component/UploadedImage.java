/**
 * 
 */
package com.jared.dotart.ui.component;

import org.vaadin.imagefilter.FilterOperation;
import org.vaadin.imagefilter.Image;
import org.vaadin.imagefilter.filters.FitIntoFilter;

import com.jared.dotart.model.ImageSettings;
import com.jared.dotart.ui.ImageSettingsEditor;
import com.vaadin.event.MouseEvents.ClickListener;
import com.vaadin.terminal.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Runo;

/**
 * This component aims to show the image, its name, its size and aims to provide component to set dot's size and space between dots
 * 
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class UploadedImage extends CustomComponent implements TimelineItem {

	private Image image;
	private Label name;
	private Embedded trash;
	private Object itemId;
	private ImageSettings settings;
	private ImageSettingsEditor editor;
	private UploadImageListener listener;
	
	public UploadedImage(ImageSettingsEditor editorP) {
		// Create the editor (use it multiple time)
		editor = editorP;

		setSizeFull();
		setHeight("70px");
		addStyleName("uploaded-image");
		GridLayout layout = new GridLayout(3, 2);
		layout.setWidth("100%");
		layout.setSpacing(false);
		// Adjust column resizing
		layout.setColumnExpandRatio(1, 1.0f);
		layout.setRowExpandRatio(1, 1.0f);
		// Add UI components	
		VerticalLayout imgLayout = new VerticalLayout();
		imgLayout.setWidth("150px");
		imgLayout.setHeight("70px");
		layout.addComponent(imgLayout,0,0,0,1);
		image = new Image(true);
		imgLayout.addComponent(image);
		imgLayout.setComponentAlignment(image, Alignment.MIDDLE_CENTER);
		name = new Label("name");
		name.addStyleName("name");
		name.setSizeUndefined();
		layout.addComponent(name, 1, 0);
		trash = new Embedded();
		trash.setSource(new ThemeResource("img/trash.png"));
		trash.setDescription("Remove the image");
		trash.addListener(new ClickListener() {
			public void click(com.vaadin.event.MouseEvents.ClickEvent event) {
				if (listener != null) {
					listener.removeClicked();
				}
			}
		});
		layout.addComponent(trash, 2, 0);
		layout.setComponentAlignment(trash, Alignment.TOP_RIGHT);
		
		Button advancedSettings = new Button("Settings...");
		advancedSettings.setWidth("100px");
		advancedSettings.setDescription("Set advanced settings");
		advancedSettings.setStyleName(Runo.BUTTON_SMALL);
		advancedSettings.addListener(new Button.ClickListener() {
			public void buttonClick(ClickEvent event) {
				editor.edit(settings);
			}
		});
		layout.addComponent(advancedSettings, 2,1);
		layout.setComponentAlignment(advancedSettings, Alignment.MIDDLE_CENTER);
		setCompositionRoot(layout);
	}
	
	/**
	 * @param listener the listener to set
	 */
	public void setListener(UploadImageListener listener) {
		this.listener = listener;
	}

	/**
	 * Constructor which initializes the filename
	 * @param n
	 */
	public UploadedImage(String n, ImageSettingsEditor editorP) {
		this(editorP);
		name.setValue(n);		
	}
	
	/**
	 * Update values
	 * @param is
	 */
	public void set(ImageSettings imgSettings) {
		image.load(imgSettings.getImage().getInputStream());
		// Fit the image to a maximum size
		FilterOperation op = FilterOperation.getByName(FilterOperation.FITINTO);
		((FitIntoFilter) op.getFilter()).setWidth(150);
		((FitIntoFilter) op.getFilter()).setHeight(70);
		image.addOperation(op);
		image.applyOperations();
		String size = ": "+image.getOriginalImage().getWidth()+" x "+image.getOriginalImage().getHeight();
		name.setValue(imgSettings.getName()+size+" "+getSize(imgSettings.getSize()));
		// Set the setting
		settings = imgSettings;
	}
		
	/* (non-Javadoc)
	 * @see com.jared.dotart.ui.component.TimelineItem#getSettings()
	 */
	public ImageSettings getSettings() {
		return settings;
	}
	
	/**
	 * Return a string representation of a file size
	 * @param s
	 * @return
	 */
	private static String getSize(long s) {
		String result = "1";
		if (s > 1000) {
			result = "" + s/1024;
		}
		return "(" + result + " Ko)";
	}

	/* (non-Javadoc)
	 * @see com.jared.dotart.ui.component.TimelineItem#getItemId()
	 */
	public Object getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(Object itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the editor
	 */
	public ImageSettingsEditor getEditor() {
		return editor;
	}
}
