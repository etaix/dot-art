package com.jared.dotart.ui.component;

import com.jared.dotart.model.ImageSettings;

public interface TimelineItem {

	/**
	 * Return the current image settings
	 * @return
	 */
	public abstract ImageSettings getSettings();

	/**
	 * @return the itemId
	 */
	public abstract Object getItemId();

}