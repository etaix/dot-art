/**
 * 
 */
package com.jared.dotart.model;

import java.io.Serializable;

import org.vaadin.imagefilter.Image;

/**
 * The bean which contains a image settings
 * @author Eric Taix (eric dot taix at gmail dot com
 */
@SuppressWarnings("serial")
public class ImageSettings implements Serializable {
	// The unique Id of the image
	private Object itemId;
	// The image itself
	private transient Image image;
	// The filename
	private String name;
	// The size in bytes
	private long size;
	// The X position of the center of the image
	private double xCenter = 0;
	// The Y position of the center of the image
	private double yCenter = 0;
	// The delay in seconds before the image explode
	private double delay = 3.5;
	// The dot size in pixels
	private double dotSize = 18;
	// The space between each dot in pixels
	private double dotSpacing = 2;
	/**
	 * @return the itemId
	 */
	public Object getItemId() {
		return itemId;
	}
	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(Object itemId) {
		this.itemId = itemId;
	}
	/**
	 * @return the image
	 */
	public Image getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the size
	 */
	public long getSize() {
		return size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(long size) {
		this.size = size;
	}
	/**
	 * @return the xCenter
	 */
	public double getxCenter() {
		return xCenter;
	}
	/**
	 * @param xCenter the xCenter to set
	 */
	public void setxCenter(double xCenter) {
		this.xCenter = xCenter;
	}
	/**
	 * @return the yCenter
	 */
	public double getyCenter() {
		return yCenter;
	}
	/**
	 * @param yCenter the yCenter to set
	 */
	public void setyCenter(double yCenter) {
		this.yCenter = yCenter;
	}
	/**
	 * @return the delay
	 */
	public double getDelay() {
		return delay;
	}
	/**
	 * @param delay the delay to set
	 */
	public void setDelay(double delay) {
		this.delay = delay;
	}
	/**
	 * @return the dotSize
	 */
	public double getDotSize() {
		return dotSize;
	}
	/**
	 * @param dotSize the dotSize to set
	 */
	public void setDotSize(double dotSize) {
		this.dotSize = dotSize;
	}
	/**
	 * @return the dotSpacing
	 */
	public double getDotSpacing() {
		return dotSpacing;
	}
	/**
	 * @param dotSpacing the dotSpacing to set
	 */
	public void setDotSpacing(double dotSpacing) {
		this.dotSpacing = dotSpacing;
	}
}
