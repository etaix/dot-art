/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import com.jared.dotart.Countdown.Type;
import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.CountdownListener;
import com.jared.dotart.event.CountdownTypeEvent;
import com.jared.dotart.event.ImageEvent;
import com.jared.dotart.event.ImageListener;
import com.jared.dotart.event.MiscellaneaousListener;
import com.jared.dotart.event.MiscellaneousEvent;
import com.jared.dotart.model.ImageSettings;
import com.jared.dotart.ui.component.UploadedTable;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.DownloadStream;

/**
 * This generator generates the sequencer.js file
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class SequencerGenerator implements Generator, CountdownListener, MiscellaneaousListener, ImageListener {

	private ApplicationContext ctx;
	private boolean hasCountdown = false;
	private boolean hasImageBeforeCountdown = false;
	private boolean hasImageAfterCountdown = false;
	
	private MiscellaneousEvent miscellaneaous = new MiscellaneousEvent();
	// Countdown event (date) to apply
	private CountdownTypeEvent countdown = new CountdownTypeEvent(Type.RELATIVE, new Date(), 15);

	/**
	 * Constructor
	 * 
	 * @param ctxP
	 */
	public SequencerGenerator(ApplicationContext ctxP) {
		ctx = ctxP;
		DotartApplication.getBlackboard().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#init()
	 */
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#generate(java.lang.String)
	 */
	public DownloadStream generate(String relativeUri) {
		if (relativeUri.equals("generator/js/sequencer.js")) {
			String name = ctx.getBaseDirectory().getAbsolutePath() + "/WEB-INF/templates/js/sequencer.stg";
			STGroup group = new STGroupFile(name, '$', '$');
			ST st = group.getInstanceOf("main");

			st.add("wa", miscellaneaous.getWatchAgainTitle());
			st.add("fm", miscellaneaous.getFinalMessage());
			st.add("fu", miscellaneaous.getFinalUrl());
			st.add("hascd", hasCountdown);
			st.add("hasbefore", hasImageBeforeCountdown);
			st.add("hasafter", hasImageAfterCountdown);

			String dateStr = "";
			if (countdown.getType().equals(Type.NONE)) {
				dateStr = "0";
			}
			else if (countdown.getType().equals(Type.RELATIVE)) {
				dateStr = "new Date().getTime()+" + countdown.getSeconds() + "*1000";
			}
			else if (countdown.getType().equals(Type.ABSOLUTE)) {
				Calendar c = Calendar.getInstance();
				c.setTime(countdown.getDate());
				dateStr = "new Date(" + c.get(Calendar.YEAR) + "," + c.get(Calendar.MONTH) + "," + c.get(Calendar.DAY_OF_MONTH) + "," + c.get(Calendar.HOUR_OF_DAY) + ","
						+ c.get(Calendar.MINUTE) + "," + c.get(Calendar.SECOND) + ").getTime()";
			}
			st.add("date", dateStr);
			DownloadStream ds = new DownloadStream(new ByteArrayInputStream(st.render().getBytes()), GeneratorHandler.getMimeType(relativeUri), null);
			ds.setCacheTime(-1);
			return ds;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.CountdownListener#countdownTypeChanged(com.jared.dotart.event.CountdownTypeEvent)
	 */
	public void countdownTypeChanged(CountdownTypeEvent evtP) {
		countdown = evtP;
	}

	/* (non-Javadoc)
	 * @see com.jared.dotart.event.MiscellaneaousListener#miscellaneaousChanged(com.jared.dotart.event.MiscellaneousEvent)
	 */
	public void miscellaneaousChanged(MiscellaneousEvent evtP) {
		miscellaneaous = evtP;
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.ImageListener#imageAdded(com.jared.dotart.event.ImageEvent)
	 */
	public void imagesChanged(ImageEvent eventP) {
		hasCountdown = false;
		hasImageBeforeCountdown = false;
		hasImageAfterCountdown = false;
		List<ImageSettings> settingList = eventP.getItems();
		for (int iLoop = 0; iLoop < settingList.size(); iLoop++) {
			ImageSettings settings = settingList.get(iLoop);
			if (settings.getItemId() == UploadedTable.COUNTDOWN_ID) {
				hasCountdown = true;
				hasImageBeforeCountdown = (iLoop > 0);
				hasImageAfterCountdown = (iLoop <  settingList.size()-1);
			}
		}
		// If no countdown has been found but if there's at least one image
		if (!hasCountdown) {
			if (settingList.size() > 0) {
				// All images are then before the countdown
				hasImageAfterCountdown = true;
			}
		}
	}
}
