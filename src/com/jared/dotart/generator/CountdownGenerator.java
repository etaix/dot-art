/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.ColorsEvent;
import com.jared.dotart.event.ColorsListener;
import com.jared.dotart.event.SettingsEvent;
import com.jared.dotart.event.SettingsListener;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.DownloadStream;

/**
 * This generator generates the CSS file
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class CountdownGenerator implements Generator, SettingsListener, ColorsListener {

	private ApplicationContext ctx;

	// The settings to apply
	private SettingsEvent settings = new SettingsEvent();
	// The colors to apply
	private ColorsEvent colors = new ColorsEvent();

	/**
	 * Constructor
	 * 
	 * @param ctxP
	 */
	public CountdownGenerator(ApplicationContext ctxP) {
		ctx = ctxP;
		DotartApplication.getBlackboard().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#init()
	 */
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#generate(java.lang.String)
	 */
	public DownloadStream generate(String relativeUri) {
		if (relativeUri.equals("generator/js/countdown.js")) {
			String name = ctx.getBaseDirectory().getAbsolutePath() + "/WEB-INF/templates/js/countdown.stg";
			STGroup group = new STGroupFile(name, '^', '^');
			ST st = group.getInstanceOf("main");

			st.add("xg", settings.getxGravity());
			st.add("yg", settings.getyGravity());
			st.add("lv", settings.getLowVelocity());
			st.add("hv", settings.getHighVelocity());
			st.add("r", settings.getRestitution() / 10);
			st.add("dc", colors.getDaysColor().substring(1));
			st.add("hc", colors.getHoursColor().substring(1));
			st.add("mc", colors.getMinutesColor().substring(1));
			st.add("sc", colors.getSecondesColors().substring(1));
			st.add("sec", colors.getSeparatorsColor().substring(1));
			st.add("oc", colors.getOtherdotColor().substring(1));
			st.add("size", settings.getSize());
			st.add("xpos", settings.getxPos());
			st.add("ypos", settings.getyPos());
	
			DownloadStream ds = new DownloadStream(new ByteArrayInputStream(st.render().getBytes()), GeneratorHandler.getMimeType(relativeUri), null);
			ds.setCacheTime(-1);
			return ds;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.SettingsListener#settingsChanged(com.jared.dotart.event.SettingsEvent)
	 */
	public void settingsChanged(SettingsEvent evtP) {
		settings = evtP;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.ColorsListener#colorChanged(com.jared.dotart.event.ColorsEvent)
	 */
	public void colorChanged(ColorsEvent evtP) {
		colors = evtP;
	}
}
