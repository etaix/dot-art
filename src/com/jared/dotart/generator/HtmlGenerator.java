/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;
import java.io.Serializable;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.MiscellaneaousListener;
import com.jared.dotart.event.MiscellaneousEvent;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.DownloadStream;

/**
 * This class generates the main HTML file
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class HtmlGenerator implements Generator, MiscellaneaousListener, Serializable {

	private ApplicationContext ctx;
    private MiscellaneousEvent miscellaneaous = new MiscellaneousEvent(); 
	
	/**
	 * Constructor
	 * 
	 * @param ctxP
	 */
	public HtmlGenerator(ApplicationContext ctxP) {
		ctx = ctxP;
		DotartApplication.getBlackboard().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#init()
	 */
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#generate(java.lang.String)
	 */
	public DownloadStream generate(String relativeUri) {
		if (relativeUri.equals("generator/countdown.html")) {
			String name = ctx.getBaseDirectory().getAbsolutePath() + "/WEB-INF/templates/countdown.stg";
			STGroup group = new STGroupFile(name, '$', '$');
			ST st = group.getInstanceOf("main");
			st.add("title", miscellaneaous.getTitle());
			DownloadStream ds = new DownloadStream(new ByteArrayInputStream(st.render().getBytes()),  GeneratorHandler.getMimeType(relativeUri), null);
			ds.setCacheTime(-1);
			return ds;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.jared.dotart.event.MiscellaneaousListener#miscellaneaousChanged(com.jared.dotart.event.MiscellaneousEvent)
	 */
	public void miscellaneaousChanged(MiscellaneousEvent evtP) {
		miscellaneaous = evtP;
	}

}
