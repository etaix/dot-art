/**
 * 
 */
package com.jared.dotart.generator;

/**
 * A dot will be display with a circle
 * @author eric
 */
public class Dot {

	// The x value
	public int x;
	// The y value
	public int y;
	// The color value (it is the color index in the color list)
	public int c;
	
	public Dot(int xP, int yP, int cP) {
		x = xP;
		y = yP;
		c = cP;
	}

	@Override
	public String toString() {
		return "["+x+","+y+","+c+"]";
	}
	
}
