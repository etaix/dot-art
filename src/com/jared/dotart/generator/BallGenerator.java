/**
 * 
 */
package com.jared.dotart.generator;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.vaadin.terminal.DownloadStream;

/**
 * This class generates dynamically teh ball draws
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class BallGenerator implements Generator {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#init()
	 */
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#generate(java.lang.String)
	 */
	public DownloadStream generate(String relativeUri) {
		if (relativeUri.startsWith("generator/img/ball")) {

			// Size is set to the maximum dot's size, so we can cache the images
			int size = 30;
//			float radius = size / 2;
			// Retrieve the color
			int index = relativeUri.lastIndexOf("-");
			String color = relativeUri.substring(index + 1, index + 1 + 6);
			Color originalColor = Color.decode("#" + color);
			// Create an image and draw the ball
			BufferedImage image = new BufferedImage(size, size, BufferedImage.TYPE_INT_ARGB);
			Graphics2D drawable = image.createGraphics();
			drawable.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

//			Color color1 = new Color(originalColor.getRed(), originalColor.getGreen(), originalColor.getBlue(), 255);
//			Color color2 = new Color(originalColor.getRed(), originalColor.getGreen(), originalColor.getBlue(), 25);
//			Point2D center = new Point2D.Float(radius, radius);
//
//			Point2D focus = new Point2D.Float(radius, radius);
//			float[] dist = { 0.0f, 0.2f, 1.0f };
//			Color[] colors = { color1, color1, color2 };
			// RadialGradientPaint p = new RadialGradientPaint(center, radius, focus, dist, colors, CycleMethod.REPEAT);
			// drawable.setPaint(p);
			// drawable.fillRect(0, 0, size, size);
			drawable.setColor(originalColor);
			drawable.fillOval(0, 0, size, size);

			try {
				// Write the image to a buffer.
				ByteArrayOutputStream imagebuffer = new ByteArrayOutputStream();
				ImageIO.write(image, "png", imagebuffer);

				// Return a stream from the buffer
				DownloadStream result = new DownloadStream(new ByteArrayInputStream(imagebuffer.toByteArray()), GeneratorHandler.getMimeType(relativeUri), null);
				result.setCacheTime(24 * 60 * 60 * 1000);
				return result;
			}
			catch (IOException e) {
				return null;
			}
		}
		return null;
	}

}
