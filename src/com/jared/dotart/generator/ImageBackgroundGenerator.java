/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.ColorsEvent;
import com.jared.dotart.event.ColorsListener;
import com.vaadin.terminal.DownloadStream;

/**
 * This generator generates the CSS file
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class ImageBackgroundGenerator implements Generator, ColorsListener {

	private static final Logger logger = Logger.getLogger(ImageBackgroundGenerator.class.getName());

	// The colors
	private ColorsEvent colors;

	/**
	 * Constructor
	 * 
	 * @param ctxP
	 */
	public ImageBackgroundGenerator() {
		DotartApplication.getBlackboard().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#init()
	 */
	public void init() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.generator.Generator#generate(java.lang.String)
	 */
	public DownloadStream generate(String relativeUri) {
		if (colors != null && colors.getBackgroundName() != null && relativeUri.endsWith(colors.getBackgroundName())) {
			String fn = colors.getBackgroundName();
			int index = fn.lastIndexOf(".");
			if (index != -1) {
				String ext = fn.substring(index + 1);
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				try {
					ImageIO.write(colors.getBackgroundImage(), ext, os);
					os.flush();
					os.close();
					DownloadStream ds = new DownloadStream( new ByteArrayInputStream(os.toByteArray()),  GeneratorHandler.getMimeType(relativeUri), null);
					ds.setCacheTime(-1);
					return ds;					
				}
				catch (IOException e) {
					logger.severe("IOException " + e.getMessage());
				}
			}
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.ColorsListener#colorChanged(com.jared.dotart.event.ColorsEvent)
	 */
	public void colorChanged(ColorsEvent evtP) {
		colors = evtP;
	}
}
