/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.ColorsEvent;
import com.jared.dotart.event.ColorsListener;
import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.URIHandler;

/**
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class DownloadHandler implements URIHandler, ColorsListener {

	// The colors to apply
	private ColorsEvent colors = new ColorsEvent();
	// The delegate : it will pratically creates files
	private GeneratorHandler delegate;
	// List of static files (most of files are dynamic but these have a static filename : images have not)
	private static final String[] urls = new String[] {
		"generator/countdown.html",
		"generator/css/style.css",
		"generator/js/sequencer.js",
		"generator/js/base.js",
		"generator/js/box2d.js",
		"generator/js/Three.js",
		"generator/js/Tween.js",
		"generator/js/countdown.js",
		"generator/js/entities.js",
		"generator/js/pre-dots.js",
		"generator/js/colors.js",
		"generator/js/dots.js"
	};

	
	public DownloadHandler(GeneratorHandler delegateP) {
		delegate = delegateP;
		DotartApplication.getBlackboard().addListener(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.vaadin.terminal.URIHandler#handleURI(java.net.URL, java.lang.String)
	 */
	public DownloadStream handleURI(URL context, String relativeUri) {
		// Catch the given URI only
		if (!relativeUri.startsWith("download")) {
			return null;
		}
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(baos);

		try {
			// Process all static files
			processStaticFiles(context, zos);
			// Process to dynamic images names
			processBallImages(context, zos);
			// Process the background image
			if (colors != null && colors.getBackgroundName() != null) {
				String backgroundName = "generator/img/"+colors.getBackgroundName();
				DownloadStream stream = delegate.handleURI(context, backgroundName);
				if (stream != null) {
					addToZip("img/"+colors.getBackgroundName(), zos, stream.getStream());
				}
			}
			// Close all stream
			zos.flush();
			baos.flush();
			zos.close();
			baos.close();
			// Build the final stream AND set no cache
			DownloadStream ds = new DownloadStream(new ByteArrayInputStream(baos.toByteArray()), "application/zip", "countdown.zip");
			ds.setCacheTime(-1);
			return ds;
		}
		catch (Exception ex) {
			return null;
		}
	}

	/**
	 * Process dynamic balls filename according to the colors used
	 * @param context
	 * @param zos
	 * @throws Exception
	 */
	private void processBallImages(URL context, ZipOutputStream zos) throws Exception {
		if (colors != null) {
			String[] colorsArray = colors.getColors();
			for (String color : colorsArray) {
				String name = "img/ball-"+color.substring(1)+".png";
				DownloadStream stream = delegate.handleURI(context, "generator/" + name);
				if (stream != null) {
					String filename = name;
					addToZip(filename, zos, stream.getStream());
				}
			}

		}
	}
	
	/**
	 * Process all static files
	 * @param zos
	 * @throws Exception 
	 */
	private void processStaticFiles(URL context, ZipOutputStream zos) throws Exception {
		for (String url : urls) {
			DownloadStream stream = delegate.handleURI(context, url);
			if (stream != null) {
				String filename = url.substring("generator/".length());
				addToZip(filename, zos, stream.getStream());
			}
		}
	}
	
	/**
	 * Add a file to a Zip
	 * @param filename
	 * @param zos
	 * @param bis
	 * @throws Exception
	 */
	private void addToZip(String filename, ZipOutputStream zos, InputStream bis) throws Exception {
		byte bytes[] = new byte[2048];
		zos.putNextEntry(new ZipEntry(filename));
		int bytesRead;
		while ((bytesRead = bis.read(bytes)) != -1) {
			zos.write(bytes, 0, bytesRead);
		}
		zos.closeEntry();
	}

	/* (non-Javadoc)
	 * @see com.jared.dotart.event.ColorsListener#colorChanged(com.jared.dotart.event.ColorsEvent)
	 */
	public void colorChanged(ColorsEvent evtP) {
		colors = evtP;		
	}

}
