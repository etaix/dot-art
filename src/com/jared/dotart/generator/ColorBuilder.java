/**
 * 
 */
package com.jared.dotart.generator;

import java.util.ArrayList;
import java.util.List;

import com.jared.dotart.generator.EntitiesGenerator.ColorModel;

/**
 * This Builder pattern, aims to build the list of different colors
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class ColorBuilder {

	// Default colors which are used with Google drawing
	private static final int[] colorsDefault = { 0x265897, 0x13acfa, 0x009a49, 0xc0000b, 0xa1cb50, 0xB11B23, 0xFDD901, 0x000000, 0xE44D26, 0x70A3D6, 0x9DC3E6, 0xAAAAAA, 0xCCCCCC,	0xEEEEEE };

	// Colors list
	private List<ColorModel> colors = new ArrayList<ColorModel>();
	
	/**
	 * Constructor which initialize the default color
	 */
	public ColorBuilder() {
		for (int i = 0; i < colorsDefault.length; i++) {
			ColorModel model = new ColorModel();
			model.color = colorsDefault[i]; 
			model.alpha = 1.0; 
			colors.add(model);
		}		
	}
	
	/**
	 * Add a color if it does not exist and returns the color index
	 * 
	 * @param colorValue
	 * @return the color index
	 */
	public int addColor(int colorValue, double alphaValue) {
		ColorModel model = new ColorModel();
		model.color = colorValue;
		model.alpha = alphaValue;
		int index = colors.indexOf(model);
		if (index == -1) {
			colors.add(model);
			return colors.size() - 1;
		}
		return index;
	}
	
	/**
	 * Returns the list of colors
	 * @return
	 */
	public List<ColorModel> getColor() {
		return colors;
	}
}
