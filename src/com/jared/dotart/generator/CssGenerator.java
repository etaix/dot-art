/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupFile;

import com.jared.dotart.DotartApplication;
import com.jared.dotart.event.ColorsEvent;
import com.jared.dotart.event.ColorsListener;
import com.jared.dotart.event.SettingsEvent;
import com.jared.dotart.event.SettingsListener;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.DownloadStream;

/**
 * This generator generates the CSS file
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class CssGenerator implements Generator, ColorsListener, SettingsListener {

	private ApplicationContext ctx;

	// The colors
	private ColorsEvent colors = new ColorsEvent();
	// Settings
	private SettingsEvent settings = new SettingsEvent();
	
	/**
	 * Constructor
	 * @param ctxP
	 */
	public CssGenerator(ApplicationContext ctxP) {
		ctx = ctxP;
		DotartApplication.getBlackboard().addListener(this);
	}
	
	/* (non-Javadoc)
	 * @see com.jared.dotart.generator.Generator#init()
	 */
	public void init() {
	}

	/* (non-Javadoc)
	 * @see com.jared.dotart.generator.Generator#generate(java.lang.String)
	 */
	public DownloadStream generate(String relativeUri) {
		if (relativeUri.equals("generator/css/style.css")) {
			String name = ctx.getBaseDirectory().getAbsolutePath() + "/WEB-INF/templates/css/style.stg";
			STGroup group = new STGroupFile(name, '$', '$');
			ST st = group.getInstanceOf("main");
			// Build the background attribut
			String backgroundcolor = "";
			if (colors != null && (colors.getBackgroundName() != null || colors.getBackgroundColor() != null)) {
				String bgcolor = colors.getBackgroundColor();
				String bgname = colors.getBackgroundName();
				backgroundcolor = "background: " + (bgcolor != null ? bgcolor : "transparent ") + (bgname != null ? " url(../img/" + bgname + ")" : "") + ";";
			}
			String backgroundRepeats = "";
			if (colors != null) {
				backgroundRepeats = "background-repeat: ";
				if (colors.isRepeatX()) {
					backgroundRepeats += " repeat-x ";
				} 
				if (colors.isRepeatY()) {
					backgroundRepeats += " repeat-y ";
				}
				if (!colors.isRepeatX() && !colors.isRepeatY()) {
					backgroundRepeats += " no-repeat ";	
				}
				backgroundRepeats += ";";
			}
			st.add("background", backgroundcolor);
			st.add("backgroundrepeat", backgroundRepeats);
			st.add("size", settings.getSize());
			DownloadStream ds = new DownloadStream(new ByteArrayInputStream(st.render().getBytes()),  GeneratorHandler.getMimeType(relativeUri), null);
			ds.setCacheTime(-1);
			return ds;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.ColorsListener#colorChanged(com.jared.dotart.event.ColorsEvent)
	 */
	public void colorChanged(ColorsEvent evtP) {
		colors = evtP;
	}

	/* (non-Javadoc)
	 * @see com.jared.dotart.event.SettingsListener#settingsChanged(com.jared.dotart.event.SettingsEvent)
	 */
	public void settingsChanged(SettingsEvent evtP) {
		settings = evtP;
		
	}
	
	
}
