/**
 * 
 */
package com.jared.dotart.generator;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.jared.dotart.DotartApplication;
import com.vaadin.service.ApplicationContext;
import com.vaadin.terminal.DownloadStream;
import com.vaadin.terminal.URIHandler;

/**
 * This class is the generator. It compiles images, colors to produce final files
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
@SuppressWarnings("serial")
public class GeneratorHandler implements URIHandler {

	private static final Logger logger = Logger.getLogger(GeneratorHandler.class.getName());
	// List of generator
	private List<Generator> generators = new ArrayList<Generator>();
	// The current application
	private DotartApplication application;

	/**
	 * Default contructor
	 * 
	 * @param app
	 */
	public GeneratorHandler(DotartApplication app) {
		application = app;
	}

	/**
	 * Add a new generator. Generators are used in the order you add them.
	 * 
	 * @param gen
	 */
	public void addGenerator(Generator gen) {
		generators.add(gen);
	}

	/**
	 * Provides the dynamic resource if the URI matches the resource URI. The matching URI is "/myresource" under the application URI context.
	 * 
	 * Returns null if the URI does not match. Otherwise returns a download stream that contains the response from the server.
	 */
	public DownloadStream handleURI(URL context, String relativeUri) {
		if (logger.isLoggable(Level.FINEST)) {
			logger.finest("Handling URL: " + relativeUri);
		}
		try {
			// Catch the given URI that identifies the resource,
			// otherwise let other URI handlers or the Application
			// to handle the response.
			if (!relativeUri.startsWith("generator")) {
				return null;
			}

			// Return a stream from the buffer.
			DownloadStream istream = null;

			// There is only one HTML file. So we consider that when handling the first request of an HTML file it's time to init generators
			if (relativeUri.endsWith("html")) {
				for (Generator generator : generators) {
					generator.init();
				}
			}

			// For each generator, try to generate the stream
			for (Generator generator : generators) {
				istream = generator.generate(relativeUri);
				if (istream != null) {
					break;
				}
			}

			// If the stream is null, it means that no generator was able to handle it
			if (istream == null) {
				ApplicationContext ctx = application.getContext();
				// Get the requested file
				String requestedFile = relativeUri.substring("generator".length());
				File rootBase = ctx.getBaseDirectory();
				String name = rootBase.getAbsolutePath() + "/WEB-INF/templates/" + requestedFile;
				File file = new File(name);
				FileInputStream fistream = new FileInputStream(file);
				// Return the stream
				DownloadStream stream = new DownloadStream(fistream, getMimeType(relativeUri), null);
				stream.setCacheTime(24*60*60*1000);
				return stream;

			}
			// Return the DownloadStream
			else {
				return istream;
			}
		}
		catch (Throwable th) {
			logger.severe("An unexpected error occured: " + th + "\n" + th.getStackTrace()[0] + "\n" + th.getStackTrace()[1] + "\n" + th.getStackTrace()[2] + "\n"
					+ th.getStackTrace()[3] + "\n" + th.getStackTrace()[4]);
			return getError(th);
		}
	}


	private DownloadStream getError(Throwable th) {
		ByteArrayInputStream bais = new ByteArrayInputStream(th.getMessage().getBytes());
		return new DownloadStream(bais, "text/html", null);
	}

	/**
	 * Returns the mimetype of a filename
	 * 
	 * @param filename
	 * @return
	 */
	public static String getMimeType(String filename) {
		if (filename != null) {
			if (filename.endsWith(".css")) {
				return "text/css";
			}
			else if (filename.endsWith(".png")) {
				return "image/png";
			}
			else if (filename.endsWith(".jpg")) {
				return "image/jpg";
			}
			else if (filename.endsWith(".jpeg")) {
				return "image/jpg";
			}
			else if (filename.endsWith(".js")) {
				return "application/javascript";
			}
			else if (filename.endsWith(".html") || filename.endsWith(".htm")) {
				return "text/html";
			}
		}
		logger.info("MimeType not defined for file: " + filename);
		return null;
	}

}
