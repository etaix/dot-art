/**
 * 
 */
package com.jared.dotart.generator;

import com.vaadin.terminal.DownloadStream;

/**
 * Defines generator witch are used by the generator handler
 * 
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public interface Generator {

	/**
	 * When starting a new generation, the Generator.init is called. An implementation can use this method to compute data
	 */
	public void init();

	/**
	 * Generates the inputstream according to the relativeUri.<br/>
	 * If the generator can't handle the relativeUri, it must return null as soon as possible
	 * 
	 * @return
	 */
	public DownloadStream generate(String relativeUri);
}
