/**
 * 
 */
package com.jared.dotart;

/**
 * This is a facade for GA utilities
 * @author Eric Taix (eric dot taix at gmail dot com)
 */
public class GAUtils {

  public static String getExternalLinkWithGA(String href, String target, String page, String caption) {
	  String targetAttr = "";
	  if (target != null && target.length()>0) {
		  targetAttr = "target='"+target+"'";
	  }
	  return "<a "+targetAttr+" href='"+href+"' onclick='javascript: var t = window._gat._getTracker(\"UA-27200430-1\");t._trackPageview(\""+page+"\");'>"+caption+"</a>";
  }
	
}
