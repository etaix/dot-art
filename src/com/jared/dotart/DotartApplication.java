package com.jared.dotart;

import java.net.URL;

import org.vaadin.addthis.AddThis;
import org.vaadin.googleanalytics.tracking.GoogleAnalyticsTracker;

import com.github.wolfie.blackboard.Blackboard;
import com.jared.dotart.event.ColorsEvent;
import com.jared.dotart.event.ColorsListener;
import com.jared.dotart.event.CountdownListener;
import com.jared.dotart.event.CountdownTypeEvent;
import com.jared.dotart.event.FeatureEvent;
import com.jared.dotart.event.FeatureListener;
import com.jared.dotart.event.ImageEvent;
import com.jared.dotart.event.ImageListener;
import com.jared.dotart.event.MenuEvent;
import com.jared.dotart.event.MenuListener;
import com.jared.dotart.event.MiscellaneaousListener;
import com.jared.dotart.event.MiscellaneousEvent;
import com.jared.dotart.event.SettingsEvent;
import com.jared.dotart.event.SettingsListener;
import com.jared.dotart.generator.BallGenerator;
import com.jared.dotart.generator.CountdownGenerator;
import com.jared.dotart.generator.CssGenerator;
import com.jared.dotart.generator.DownloadHandler;
import com.jared.dotart.generator.EntitiesGenerator;
import com.jared.dotart.generator.GeneratorHandler;
import com.jared.dotart.generator.HtmlGenerator;
import com.jared.dotart.generator.ImageBackgroundGenerator;
import com.jared.dotart.generator.SequencerGenerator;
import com.jared.dotart.terminal.BrowseHandler;
import com.jared.dotart.ui.AboutPage;
import com.jared.dotart.ui.CreatePage;
import com.jared.dotart.ui.HomePage;
import com.jared.dotart.ui.component.ArtDirectory;
import com.jared.dotart.ui.component.Toolbar;
import com.vaadin.Application;
import com.vaadin.service.ApplicationContext.TransactionListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.UriFragmentUtility;
import com.vaadin.ui.UriFragmentUtility.FragmentChangedEvent;
import com.vaadin.ui.UriFragmentUtility.FragmentChangedListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class DotartApplication extends Application implements TransactionListener, MenuListener {

	private static final String URI_ABOUT = "about";
	private static final String URI_BROWSE = "browse";
	private static final String URI_CREATE = "create";
	private static final String URI_HOME = "home";
	private static final long serialVersionUID = 1L;
	// This thread local will contain the blackboard instance
	private static ThreadLocal<Blackboard> BLACKBOARD = new ThreadLocal<Blackboard>();
	// The current user instance
	private final Blackboard blackboardInstance = new Blackboard();
	// The tracker instance
	private GoogleAnalyticsTracker googleTracker;
	// The main layout which contains all contents
	private VerticalLayout mainLayout;
	// The content used when the user creates a new art
	private VerticalLayout createContent;
	// The content used for the home page
	private VerticalLayout homeContent;
	// The current showned content
	private VerticalLayout currentContent;
	private VerticalLayout aboutContent;
	// The generator
	private GeneratorHandler generator;
	// The browse/directory content
	private VerticalLayout browseContent;
	// URI Fragment utility
	private UriFragmentUtility uriFragmentUtility = new UriFragmentUtility();
	// The toolbar
	private Toolbar toolbar;

	@SuppressWarnings("serial")
	@Override
	public void init() {
		// Initialize the venet bus system
		initEventBus(); 
		// Register myself as a menu listener
		getBlackboard().addListener(this);

		// Create the Generator
		generator = new GeneratorHandler(this);
		generator.addGenerator(new HtmlGenerator(getContext()));
		generator.addGenerator(new EntitiesGenerator());
		generator.addGenerator(new CssGenerator(getContext()));
		generator.addGenerator(new ImageBackgroundGenerator());
		generator.addGenerator(new BallGenerator());
		generator.addGenerator(new CountdownGenerator(getContext()));
		generator.addGenerator(new SequencerGenerator(getContext()));
		// Create the downloader
		DownloadHandler downloader = new DownloadHandler(generator);

		// Set the current theme
		setTheme("dotart");
		Window mainWindow = new Window("Dot-Art");
		// Let the generator handles some URI (preview)
		mainWindow.addURIHandler(generator);
		// Let the downloader handles the download URI
		mainWindow.addURIHandler(downloader);
		// Let the browser handles some URI
		mainWindow.addURIHandler(new BrowseHandler(this));

		mainWindow.setSizeFull();
		mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(false);
		mainLayout.setSpacing(true);

		toolbar = new Toolbar("img/logo-art2.png", new String[] { "Home", "Create art", "Browse arts", "About" }, new String[] { "home",
				"create", "browse", "about" });
		mainLayout.addComponent(toolbar);

		// -------- Contents -----------


		// The home content
		homeContent = new VerticalLayout();
		homeContent.setMargin(false);
		homeContent.setSizeFull();
		HomePage homePage = new HomePage();
		homeContent.addComponent(homePage);
		homeContent.setComponentAlignment(homePage, Alignment.MIDDLE_CENTER);

		// The create content
		createContent = new VerticalLayout();
		createContent.setMargin(false);
		createContent.setSizeFull();
		CreatePage createPage = new CreatePage();
		createContent.addComponent(createPage);
		createContent.setComponentAlignment(createPage, Alignment.TOP_CENTER);
		
		// The about content
		aboutContent = new VerticalLayout();
		aboutContent.setSizeFull();
		AboutPage aboutTab = new AboutPage();
		aboutContent.addComponent(aboutTab);
		aboutContent.setComponentAlignment(aboutTab, Alignment.TOP_CENTER);

		// The browse panel
		browseContent = new VerticalLayout();
		browseContent.setSizeFull();
		ArtDirectory artDirectory = new ArtDirectory();
		browseContent.addComponent(artDirectory);
		browseContent.setComponentAlignment(artDirectory, Alignment.TOP_CENTER);

		// Set the current content
		currentContent = new VerticalLayout();
		currentContent.setMargin(false);
		currentContent.setSpacing(false);
		currentContent.setSizeFull();
		mainLayout.addComponent(currentContent);
		mainLayout.setExpandRatio(currentContent, 1.0f);

		// --------- Footer ----------
		VerticalLayout footerLayout = new VerticalLayout();
		footerLayout.addStyleName("footer");
		//footerLayout.setHeight("100px");
		footerLayout.setWidth("80%");
		footerLayout.setSpacing(false);
		footerLayout.setMargin(false);
		mainLayout.addComponent(footerLayout);
		mainLayout.setComponentAlignment(footerLayout, Alignment.TOP_CENTER);

		// AddThis footer
		HorizontalLayout shareLayout = new HorizontalLayout();
		shareLayout.setHeight("32px");
		shareLayout.setWidth("100%");
		footerLayout.addComponent(shareLayout);
		AddThis addThis = new AddThis("http://dot-art.cloudfoundry.com", "Generate your own html5 countdown with DotArt");
		addThis.setWidth("160px");
		addThis.setUser("dotart.info@gmail.com");
		addThis.addStyleName("addthis_32x32_style");
		addThis.addButton("facebook");
		addThis.addButton("twitter");
		addThis.addButton("google");
		addThis.addButton("email");
		shareLayout.addComponent(addThis);
		Label shareLabel = new Label("Do you like this tool ? Share it with your friends and contacts.");
		shareLabel.addStyleName("share");
		shareLayout.addComponent(shareLabel);
		shareLayout.setExpandRatio(shareLabel, 1.0f);
		shareLayout.setComponentAlignment(shareLabel, Alignment.BOTTOM_LEFT);

		// Copyright Label
		Label copyright = new Label("Created by Eric Taix © 2011");
		copyright.addStyleName("copyright");
		copyright.setWidth("180px");
		footerLayout.addComponent(copyright);
		footerLayout.setComponentAlignment(copyright, Alignment.TOP_RIGHT);
		footerLayout.setExpandRatio(copyright, 1.0f);

		mainWindow.setContent(mainLayout);
		setMainWindow(mainWindow);

		// Manage URIs
		uriFragmentUtility.setImmediate(true);
		mainLayout.addComponent(uriFragmentUtility);
		uriFragmentUtility.addListener(new FragmentChangedListener() {
			public void fragmentChanged(FragmentChangedEvent source) {
				String fragment = source.getUriFragmentUtility().getFragment();
				if (fragment != null && fragment.contains("/")) {
					String[] parts = fragment.split("/");
					fragment = parts[parts.length - 1];
				}
				// Change the view according to the fragment
				setFeature(fragment);
			}
		});
		// Add the google tracker
		googleTracker = new GoogleAnalyticsTracker("UA-27200430-1", getDomain());
		mainWindow.addComponent(googleTracker);
		// Set the default feature (home page) visible
		setFeature(URI_HOME);
		// requestInvitationCode();
	}

	/**
	 * Get the current domain: workaround for Google Analytics while developping with localhost
	 * 
	 * @return
	 */
	private String getDomain() {
		final URL url = getURL();
		if (url == null) {
			return "none";
		}
		final String host = url.getHost();
		if (host == null) {
			return "none";
		}
		return host.contains("mydomain.com") ? "mydomain.com" : "none";
	}

	/**
	 * Initialise the event bus system
	 */
	private void initEventBus() {
		// Needs to be set manually for the first transaction
		BLACKBOARD.set(blackboardInstance);
		// The application will be transactionnal aware
		getContext().addTransactionListener(this);
		// Register listeners and event pairs
		blackboardInstance.register(FeatureListener.class, FeatureEvent.class);
		blackboardInstance.register(MenuListener.class, MenuEvent.class);
		blackboardInstance.register(CountdownListener.class, CountdownTypeEvent.class);
		blackboardInstance.register(ImageListener.class, ImageEvent.class);
		blackboardInstance.register(ColorsListener.class, ColorsEvent.class);
		blackboardInstance.register(SettingsListener.class, SettingsEvent.class);
		blackboardInstance.register(MiscellaneaousListener.class, MiscellaneousEvent.class);
	}

	/**
	 * A new transaction (request) is starting
	 */
	public void transactionStart(final Application application, final Object transactionData) {
		if (application == this) {
			BLACKBOARD.set(blackboardInstance);
		}
	}

	/**
	 * The current transaction (request) is finished
	 */
	public void transactionEnd(final Application application, final Object transactionData) {
		if (application == this) {
			// to avoid keeping an Application hanging, and mitigate the
			// possibility of user session crosstalk
			BLACKBOARD.set(null);
		}
	}

	/**
	 * Conveniente method to retrieve the current blackboard instance
	 * 
	 * @return
	 */
	public static Blackboard getBlackboard() {
		return BLACKBOARD.get();
	}

	/**
	 * Set the actual feature
	 * 
	 * @param id
	 */
	private void setFeature(String featureP) {
		currentContent.removeAllComponents();
		if (URI_CREATE.equals(featureP)) {
			currentContent.addComponent(createContent);
		} else if (URI_BROWSE.equals(featureP)) {
			currentContent.addComponent(browseContent);
		} else if (URI_ABOUT.equals(featureP)) {
			currentContent.addComponent(aboutContent);
		} else {
			currentContent.addComponent(homeContent);
			featureP = URI_HOME;
		}
		toolbar.setMenu(featureP);
		// Track it
		trackView(featureP);
	}

	/**
	 * Track into Google Analytics a view
	 * @param viewName
	 */
	private void trackView(String viewName) {
		googleTracker.trackPageview("/"+viewName);	
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jared.dotart.event.MenuListener#menuChanged(com.jared.dotart.event.MenuEvent)
	 */
	public void menuChanged(MenuEvent event) {
		String fragment = URI_HOME;
		if (event != null) {
			fragment = event.getFeature();
		}
		// Set the URI fragment
		uriFragmentUtility.setFragment(fragment);
	}

	@SuppressWarnings("serial")
	public void requestInvitationCode() {
		// Create the window...
		final Window subwindow = new Window("Information");
		// ...and make it modal
		subwindow.setClosable(false);
		subwindow.setModal(true);
		subwindow.setWidth("800px");
		subwindow.setHeight("300px");
		// Configure the windws layout; by default a VerticalLayout
		VerticalLayout layout = (VerticalLayout) subwindow.getContent();
		layout.setMargin(true);
		layout.setSpacing(true);
		layout.setSizeFull();
		// Add some content; a label and a close-button
		Label message = new Label("You need an invitation code to continue", Label.CONTENT_XHTML);
		layout.addComponent(message);
		// layout.setExpandRatio(message, 1.0f);
		message.setSizeUndefined();
		message.setStyleName("home1");
		layout.setComponentAlignment(message, Alignment.MIDDLE_CENTER);
		final PasswordField invit = new PasswordField();
		invit.addStyleName("homebtn");
		layout.addComponent(invit);
		layout.setComponentAlignment(invit, Alignment.MIDDLE_CENTER);
		Button okBtn = new Button("Enter", new ClickListener() {
			// inline click-listener
			public void buttonClick(ClickEvent event) {
				if (invit != null && invit.getValue().equals("jug-mtp-34")) {
					// close the window by removing it from the parent window
					(subwindow.getParent()).removeWindow(subwindow);
				} else {
					invit.setValue("");
				}
			}
		});
		okBtn.addStyleName("homebtn");
		// The components added to the window are actually added to the window's
		// layout; you can use either. Alignments are set using the layout
		layout.addComponent(okBtn);
		layout.setComponentAlignment(okBtn, Alignment.BOTTOM_CENTER);

		if (subwindow.getParent() == null) {
			getMainWindow().addWindow(subwindow);
		}
	}

}
