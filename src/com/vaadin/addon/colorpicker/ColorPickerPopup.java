package com.vaadin.addon.colorpicker;

import java.awt.Color;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.vaadin.addon.colorpicker.ColorPicker.ColorChangeListener;
import com.vaadin.addon.colorpicker.ColorPicker.Coordinates2Color;
import com.vaadin.addon.colorpicker.events.ColorChangeEvent;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Slider.ValueOutOfBoundsException;

/**
 * The Class ColorPickerPopup.
 * 
 * @author John Ahlroos / ITMill Oy
 */
@SuppressWarnings("serial")
public class ColorPickerPopup extends Window implements ClickListener,
        ColorChangeListener, ColorSelector {

    private static final String STYLENAME = "v-colorpicker-popup-dotart";

    private static final Method COLOR_CHANGE_METHOD;
    static {
        try {
            COLOR_CHANGE_METHOD = ColorChangeListener.class.getDeclaredMethod(
                    "colorChanged", new Class[] { ColorChangeEvent.class });
        } catch (final java.lang.NoSuchMethodException e) {
            // This should never happen
            throw new java.lang.RuntimeException(
                    "Internal error finding methods in ColorPicker");
        }
    }

    /** The tabs. */
    private final TabSheet tabs = new TabSheet();

    private final Component rgbTab, hsvTab, swatchesTab;

    /** The layout. */
    private final VerticalLayout layout = new VerticalLayout();

    /** The ok button. */
    private final Button ok = new Button("OK");

    /** The cancel button. */
    private final Button cancel = new Button("Cancel");

    /** The resize button. */
    private final Button resize = new Button("...");

    /** The selected color. */
    private Color selectedColor = Color.WHITE;

    /** The history. */
    private final ColorPickerHistory history;

    /** The history container. */
    private final Layout historyContainer;

    /** The rgb gradient. */
    private ColorPickerGradient rgbGradient;

    /** The hsv gradient. */
    private ColorPickerGradient hsvGradient;

    /** The red slider. */
    private Slider redSlider;

    /** The green slider. */
    private Slider greenSlider;

    /** The blue slider. */
    private Slider blueSlider;

    /** The hue slider. */
    private Slider hueSlider;

    /** The saturation slider. */
    private Slider saturationSlider;

    /** The value slider. */
    private Slider valueSlider;

    /** The preview on the rgb tab. */
    private final ColorPickerPreview rgbPreview;

    /** The preview on the hsv tab. */
    private final ColorPickerPreview hsvPreview;

    /** The preview on the swatches tab. */
    private final ColorPickerPreview selPreview;

    /** The color select. */
    private ColorPickerSelect colorSelect;

    /** The selectors. */
    private final Set<ColorSelector> selectors = new HashSet<ColorSelector>();

    /**
     * Instantiates a new color picker popup.
     */
    public ColorPickerPopup(Color initialColor) {
        super();

        selectedColor = initialColor;

        setWidth("250px");
        setScrollable(false);
        setStyleName(STYLENAME);
        setResizable(false);
        setImmediate(true);

        // Create the history
        history = new ColorPickerHistory();
        history.addListener((ColorChangeListener) this);

        // Create the preview on the rgb tab
        rgbPreview = new ColorPickerPreview(selectedColor);
        rgbPreview.setWidth("220px");
        rgbPreview.setHeight("20px");
        rgbPreview.addListener((ColorChangeListener) this);
        selectors.add(rgbPreview);

        // Create the preview on the hsv tab
        hsvPreview = new ColorPickerPreview(selectedColor);
        hsvPreview.setWidth("220px");
        hsvPreview.setHeight("20px");
        hsvPreview.addListener((ColorChangeListener) this);
        selectors.add(hsvPreview);

        // Create the preview on the swatches tab
        selPreview = new ColorPickerPreview(selectedColor);
        selPreview.setWidth("220px");
        selPreview.setHeight("20px");
        selPreview.addListener((ColorChangeListener) this);
        selectors.add(selPreview);

        // Set the layout
        layout.setSpacing(false);
        layout.setSizeFull();
        setContent(layout);

        // Create the tabs
        rgbTab = createRGBTab(selectedColor);
        tabs.addTab(rgbTab, "RGB2", null);

        hsvTab = createHSVTab(selectedColor);
        tabs.addTab(hsvTab, "HSV", null);

        swatchesTab = createSelectTab();
        tabs.addTab(swatchesTab, "Swatches", null);

        // Add the tabs
        tabs.setWidth("100%");

        layout.addComponent(tabs);

        // Add the history
        history.setWidth("97%");
        history.setHeight("27px");

        // Create the default colors
        List<Color> defaultColors = new ArrayList<Color>();
        defaultColors.add(Color.BLACK);
        defaultColors.add(Color.WHITE);

        // Create the history
        VerticalLayout innerContainer = new VerticalLayout();
        innerContainer.setSizeFull();
        innerContainer.addComponent(history);
        innerContainer.setExpandRatio(history, 1);

        VerticalLayout outerContainer = new VerticalLayout();
        outerContainer.setWidth("99%");
        outerContainer.setHeight("27px");
        outerContainer.addComponent(innerContainer);
        historyContainer = outerContainer;

        layout.addComponent(historyContainer);

        // Add the resize button for the history
        resize.addListener(this);
        resize.setData(new Boolean(false));
        resize.setWidth("100%");
        resize.setHeight("10px");
        resize.setStyleName("resize-button");
        layout.addComponent(resize);

        // Add the buttons
        ok.setWidth("100px");
        ok.addListener(this);

        cancel.setWidth("100px");
        cancel.addListener(this);

        HorizontalLayout buttons = new HorizontalLayout();
        buttons.addComponent(ok);
        buttons.addComponent(cancel);
        buttons.setWidth("100%");
        buttons.setHeight("30px");
        buttons.setComponentAlignment(ok, Alignment.MIDDLE_CENTER);
        buttons.setComponentAlignment(cancel, Alignment.MIDDLE_CENTER);
        layout.addComponent(buttons);

        setHeight(calculateHeight());
    }

    /**
     * Calculates the height of the popup menu
     * 
     * @return Returns the height in CSS string representation
     */
    private String calculateHeight() {
        int historyHeight = historyContainer.isVisible() ? (int) historyContainer
                .getHeight()
                : 0;
        int tabsHeight = tabs.areTabsHidden() ? 0 : 32;
        int contentHeight = 370;
        int buttonsHeight = 100;
        int previewHeight = rgbPreview.isVisible() ? 20 : 0;
        return (historyHeight + tabsHeight + contentHeight + buttonsHeight
                + previewHeight + 10)
                + "px";
    }

    /**
     * Creates the rgb tab.
     * 
     * @return the component
     */
    private Component createRGBTab(Color color) {
        VerticalLayout rgbLayout = new VerticalLayout();
        rgbLayout.setMargin(false, false, true, false);
        rgbLayout.addComponent(rgbPreview);

        // Implement the RGB color converter
        Coordinates2Color RGBConverter = new Coordinates2Color() {
            public Color calculate(int x, int y) {
                float h = (x / 220f);
                float s = 1f;
                float v = 1f;

                if (y < 110) {
                    s = y / 110f;
                } else if (y > 110) {
                    v = 1f - (y - 110f) / 110f;
                }

                return new Color(Color.HSBtoRGB(h, s, v));
            }

            public int[] calculate(Color c) {

                float hsv[] = new float[3];
                Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsv);

                int x = Math.round(hsv[0] * 220f);
                int y = 0;

                // lower half
                if (hsv[1] == 1f) {
                    y = Math.round(110f - (hsv[1] + hsv[2]) * 110f);
                } else {
                    y = Math.round(hsv[1] * 110f);
                }

                return new int[] { x, y };
            }
        };

        // Add the RGB color gradient
        rgbGradient = new ColorPickerGradient("rgb-gradient", RGBConverter);
        rgbGradient.setColor(color);
        rgbGradient.addListener(this);
        rgbLayout.addComponent(rgbGradient);
        selectors.add(rgbGradient);

        // Add the RGB sliders
        VerticalLayout sliders = new VerticalLayout();
        sliders.setStyleName("rgb-sliders");

        redSlider = new Slider("Red", 0, 255);
        try {
            redSlider.setValue(color.getRed());
        } catch (ValueOutOfBoundsException e) {
        }

        redSlider.setImmediate(true);
        redSlider.setWidth("220px");
        redSlider.setStyleName("rgb-slider");
        redSlider.addStyleName("red");
        redSlider.addListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                double red = (Double) event.getProperty().getValue();
                Color newColor = new Color((int) red, selectedColor.getGreen(),
                        selectedColor.getBlue());
                setColor(newColor);
            }
        });

        sliders.addComponent(redSlider);

        greenSlider = new Slider("Green", 0, 255);

        try {
            greenSlider.setValue(color.getGreen());
        } catch (ValueOutOfBoundsException e) {
        }

        greenSlider.setStyleName("rgb-slider");
        greenSlider.addStyleName("green");
        greenSlider.setWidth("220px");
        greenSlider.setImmediate(true);
        greenSlider.addListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                double green = (Double) event.getProperty().getValue();
                Color newColor = new Color(selectedColor.getRed(), (int) green,
                        selectedColor.getBlue());
                setColor(newColor);
            }
        });
        sliders.addComponent(greenSlider);

        blueSlider = new Slider("Blue", 0, 255);

        try {
            blueSlider.setValue(color.getBlue());
        } catch (ValueOutOfBoundsException e) {
        }

        blueSlider.setStyleName("rgb-slider");
        blueSlider.setStyleName("blue");
        blueSlider.setImmediate(true);
        blueSlider.setWidth("220px");
        blueSlider.addListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                double blue = (Double) event.getProperty().getValue();
                Color newColor = new Color(selectedColor.getRed(),
                        selectedColor.getGreen(), (int) blue);
                setColor(newColor);
            }
        });
        sliders.addComponent(blueSlider);

        rgbLayout.addComponent(sliders);

        return rgbLayout;
    }

    /**
     * Creates the hsv tab.
     * 
     * @return the component
     */
    private Component createHSVTab(Color color) {
        VerticalLayout hsvLayout = new VerticalLayout();
        hsvLayout.setMargin(false, false, true, false);
        hsvLayout.addComponent(hsvPreview);

        // Implement the HSV color converter
        Coordinates2Color HSVConverter = new Coordinates2Color() {
            public int[] calculate(Color c) {

                float hsv[] = new float[3];
                Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsv);

                // Calculate coordinates
                int x = Math.round(hsv[2] * 220.0f);
                int y = Math.round(220 - hsv[1] * 220.0f);

                // Create background color of clean color
                Color bgColor = new Color(Color.HSBtoRGB(hsv[0], 1f, 1f));
                hsvGradient.setBackgroundColor(bgColor);

                return new int[] { x, y };
            }

            public Color calculate(int x, int y) {
                float saturation = 1f - (y / 220.0f);
                float value = (x / 220.0f);
                float hue = Float.parseFloat(hueSlider.getValue().toString()) / 360f;

                Color color = new Color(Color.HSBtoRGB(hue, saturation, value));
                return color;
            }
        };

        // Add the hsv gradient
        hsvGradient = new ColorPickerGradient("hsv-gradient", HSVConverter);
        hsvGradient.setColor(color);
        hsvGradient.addListener(this);
        hsvLayout.addComponent(hsvGradient);
        selectors.add(hsvGradient);

        // Add the hsv sliders
        float hsv[] = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsv);
        VerticalLayout sliders = new VerticalLayout();
        sliders.setStyleName("hsv-sliders");

        hueSlider = new Slider("Hue", 0, 360);
        try {
            hueSlider.setValue(hsv[0]);
        } catch (ValueOutOfBoundsException e1) {
        }

        hueSlider.setStyleName("hsv-slider");
        hueSlider.addStyleName("hue-slider");
        hueSlider.setWidth("220px");
        hueSlider.setImmediate(true);
        hueSlider.addListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                float hue = (Float.parseFloat(event.getProperty().getValue()
                        .toString())) / 360f;
                float saturation = (Float.parseFloat(saturationSlider
                        .getValue().toString())) / 100f;
                float value = (Float.parseFloat(valueSlider.getValue()
                        .toString())) / 100f;

                // Set the color
                Color color = new Color(Color.HSBtoRGB(hue, saturation, value));
                setColor(color);

                /*
                 * Set the background color of the hue gradient. This has to be
                 * done here since in the conversion the base color information
                 * is lost when color is black/white
                 */
                Color bgColor = new Color(Color.HSBtoRGB(hue, 1f, 1f));
                hsvGradient.setBackgroundColor(bgColor);
            }
        });
        sliders.addComponent(hueSlider);

        saturationSlider = new Slider("Saturation", 0, 100);

        try {
            saturationSlider.setValue(hsv[1]);
        } catch (ValueOutOfBoundsException e1) {
        }

        saturationSlider.setStyleName("hsv-slider");
        saturationSlider.setWidth("220px");
        saturationSlider.setImmediate(true);
        saturationSlider.addListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                float hue = (Float.parseFloat(hueSlider.getValue().toString())) / 360f;
                float saturation = (Float.parseFloat(event.getProperty()
                        .getValue().toString())) / 100f;
                float value = (Float.parseFloat(valueSlider.getValue()
                        .toString())) / 100f;
                setColor(new Color(Color.HSBtoRGB(hue, saturation, value)));
            }
        });
        sliders.addComponent(saturationSlider);

        valueSlider = new Slider("Value", 0, 100);

        try {
            valueSlider.setValue(hsv[2]);
        } catch (ValueOutOfBoundsException e1) {
        }

        valueSlider.setStyleName("hsv-slider");
        valueSlider.setWidth("220px");
        valueSlider.setImmediate(true);
        valueSlider.addListener(new ValueChangeListener() {
            public void valueChange(ValueChangeEvent event) {
                float hue = (Float.parseFloat(hueSlider.getValue().toString())) / 360f;
                float saturation = (Float.parseFloat(saturationSlider
                        .getValue().toString())) / 100f;
                float value = (Float.parseFloat(event.getProperty().getValue()
                        .toString())) / 100f;

                setColor(new Color(Color.HSBtoRGB(hue, saturation, value)));
            }
        });

        sliders.addComponent(valueSlider);
        hsvLayout.addComponent(sliders);

        return hsvLayout;
    }

    /**
     * Creates the select tab.
     * 
     * @return the component
     */
    private Component createSelectTab() {
        VerticalLayout selLayout = new VerticalLayout();
        selLayout.setMargin(false, false, true, false);
        selLayout.addComponent(selPreview);

        colorSelect = new ColorPickerSelect();
        colorSelect.addListener((ColorChangeListener) this);
        selLayout.addComponent(colorSelect);

        return selLayout;
    }

    /*
     * (non-Javadoc)
     * 
     * @seecom.vaadin.ui.Button.ClickListener#buttonClick(com.vaadin.ui.Button.
     * ClickEvent)
     */
    public void buttonClick(ClickEvent event) {
        // History resize was clicked
        if (event.getButton() == resize) {
            boolean state = (Boolean) resize.getData();

            // minimize
            if (state) {
                historyContainer.setHeight("27px");
                history.setHeight("27px");

                // maximize
            } else {
                historyContainer.setHeight("90px");
                history.setHeight("80px");
            }

            setHeight(calculateHeight());

            resize.setData(new Boolean(!state));
        }

        // Ok button was clicked
        else if (event.getButton() == ok) {
            history.setColor(getColor());
            fireColorChanged();
            close();
        }

        // Cancel button was clicked
        else if (event.getButton() == cancel) {
            close();
        }

    }

    /**
     * Notifies the listeners that the color changed
     */
    public void fireColorChanged() {
        fireEvent(new ColorChangeEvent(this, getColor()));
    }

    /**
     * Gets the history.
     * 
     * @return the history
     */
    public ColorPickerHistory getHistory() {
        return history;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vaadin.colorpicker.ColorSelector#setColor(java.awt.Color)
     */
    public void setColor(Color color) {
        if (color == null) {
            return;
        }

        selectedColor = color;

        hsvGradient.setColor(selectedColor);
        hsvPreview.setColor(selectedColor);

        rgbGradient.setColor(selectedColor);
        rgbPreview.setColor(selectedColor);

        selPreview.setColor(selectedColor);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vaadin.colorpicker.ColorSelector#getColor()
     */
    public Color getColor() {
        return selectedColor;
    }

    /**
     * Gets the color history.
     * 
     * @return the color history
     */
    public List<Color> getColorHistory() {
        return Collections.unmodifiableList(history.getHistory());
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vaadin.colorpicker.ColorSelector.ColorChangeListener#changed(com.
     * vaadin.colorpicker.ColorSelector, java.awt.Color)
     */
    public void colorChanged(ColorChangeEvent event) {
        selectedColor = event.getColor();

        try {
            redSlider.setValue(selectedColor.getRed());
            blueSlider.setValue(selectedColor.getBlue());
            greenSlider.setValue(selectedColor.getGreen());

            float hsv[] = new float[3];
            Color.RGBtoHSB(selectedColor.getRed(), selectedColor.getGreen(),
                    selectedColor.getBlue(), hsv);

            hueSlider.setValue(hsv[0] * 360f);
            saturationSlider.setValue(hsv[1] * 100f);
            valueSlider.setValue(hsv[2] * 100f);

        } catch (ValueOutOfBoundsException e) {
            e.printStackTrace();
        }

        for (ColorSelector s : selectors) {
            if (event.getSource() != s && s != this
                    && s.getColor() != selectedColor) {
                s.setColor(selectedColor);
            }
        }
    }

    /**
     * 
     * @param listener
     */
    public void addListener(ColorChangeListener listener) {
        addListener(ColorChangeEvent.class, listener, COLOR_CHANGE_METHOD);
    }

    /**
     * 
     * @param listener
     */
    public void removeListener(ColorChangeListener listener) {
        removeListener(ColorChangeEvent.class, listener);
    }

    /**
     * Is the tab visible
     * 
     * @param tab
     *            The tab to check
     * @return
     */
    private boolean tabIsVisible(Component tab) {
        Iterator<Component> tabIterator = tabs.getComponentIterator();
        while (tabIterator.hasNext()) {
            if (tabIterator.next() == tab) {
                return true;
            }
        }
        return false;
    }

    /**
     * How many tabs are visible
     * 
     * @return The number of tabs visible
     */
    private int tabsNumVisible() {
        Iterator<Component> tabIterator = tabs.getComponentIterator();
        int tabCounter = 0;
        while (tabIterator.hasNext()) {
            tabIterator.next();
            tabCounter++;
        }
        return tabCounter;
    }

    /**
     * Checks if tabs are needed and hides them if not
     */
    private void checkIfTabsNeeded() {
        if (tabsNumVisible() == 1) {
            tabs.hideTabs(true);
            setHeight(calculateHeight());
        } else {
            tabs.hideTabs(false);
            setHeight(calculateHeight());
        }
    }

    /**
     * Set RGB tab visibility
     * 
     * @param visible
     *            The visibility of the RGB tab
     */
    public void setRGBTabVisible(boolean visible) {
        if (visible && !tabIsVisible(rgbTab)) {
            tabs.addTab(rgbTab, "RGB", null);
            checkIfTabsNeeded();
        } else if (!visible && tabIsVisible(rgbTab)) {
            tabs.removeComponent(rgbTab);
            checkIfTabsNeeded();
        }
    }

    /**
     * Set HSV tab visibility
     * 
     * @param visible
     *            The visibility of the HSV tab
     */
    public void setHSVTabVisible(boolean visible) {
        if (visible && !tabIsVisible(hsvTab)) {
            tabs.addTab(hsvTab, "HSV", null);
            checkIfTabsNeeded();
        } else if (!visible && tabIsVisible(hsvTab)) {
            tabs.removeComponent(hsvTab);
            checkIfTabsNeeded();
        }
    }

    /**
     * Set Swatches tab visibility
     * 
     * @param visible
     *            The visibility of the Swatches tab
     */
    public void setSwatchesTabVisible(boolean visible) {
        if (visible && !tabIsVisible(swatchesTab)) {
            tabs.addTab(swatchesTab, "Swatches", null);
            checkIfTabsNeeded();
        } else if (!visible && tabIsVisible(swatchesTab)) {
            tabs.removeComponent(swatchesTab);
            checkIfTabsNeeded();
        }
    }

    /**
     * Set the History visibility
     * 
     * @param visible
     */
    public void setHistoryVisible(boolean visible) {
        historyContainer.setVisible(visible);
        resize.setVisible(visible);
        setHeight(calculateHeight());
    }

    /**
     * Set the preview visibility
     * 
     * @param visible
     */
    public void setPreviewVisible(boolean visible) {
        hsvPreview.setVisible(visible);
        rgbPreview.setVisible(visible);
        selPreview.setVisible(visible);
        setHeight(calculateHeight());
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.vaadin.ui.Panel#attach()
     */
    @Override
    public void attach() {
        setHeight(calculateHeight());
    }
}