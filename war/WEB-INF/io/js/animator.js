io.injectScripts(['js/countdown.js', 'js/entities.js'],
  function() {
	entitiesLoader.load('js/pre-dots.js','js/pre-colors.js', function() {
	  countdownTo(new Date().getTime()+140000, function() {
		  entitiesLoader.load('js/dots.js','js/colors.js');
	  });
	});
  });
 